
pragma solidity ^0.5.10;        

// ----------------------------------------------------------------------------------------------
// Developer Nechesov Andrey: Facebook.com/Nechesov      
// ----------------------------------------------------------------------------------------------

contract TetherToken {
    function balanceOf(address who) external view returns (uint256);    
    function transfer(address to, uint256 value) public returns (bool);
    function transferFrom(address from, address to, uint256 value) public returns (bool);
    function allowance(address _owner, address _spender) external view returns (uint256);
}

contract Staking {      

    using SafeMath for uint;
    
    TetherToken tether_token = TetherToken(0x4147FDBCBDD4A1F42DB03D6A199A4CD924A8642C23); 

    struct TContract {                        
        uint time_update; 
        bool status;            
    }

    struct Investor {                        
        uint amount; 
        uint time_update;            
    }   

    uint public invest_bank; // total money from investors
    uint public reward_bank; // reward bank for investors            
    uint public reward_bank2; // reward bank for investors when started reward update function            
    bool public rewards_not_update;     
    
    address[] public inv;
    uint public user_id_last = 0;
    uint public users_step = 100;   
    address public owner;        

    uint public min_amount;             

    mapping(address => Investor) investors;                 
    mapping(address => TContract) contracts; // trusted contracts                     

    constructor() public{        
        reward_bank = 0;
        reward_bank2 = 0;
        invest_bank = 0;
        min_amount = 10**6;
        rewards_not_update = true;
        owner = msg.sender;   //set Owner address  
        contracts[msg.sender] = TContract(now, true);
        contracts[address(this)] = TContract(now, true);        
    }

    function set_min_amount(uint _amount) public onlyOwner {
        min_amount = _amount;                
    }        

    function trusted_contracts_add(address _address) public onlyOwner {
        contracts[_address] = TContract(now, true);        
    }

    function trusted_contracts_delete(address _address) public onlyOwner {
        contracts[_address] = TContract(now, false);                
    }

    function trusted_contracts_info(address _address) public view 
            returns (
                uint time_update,                    
                bool status) {
       TContract memory tcontract = contracts[_address];
        return(
            tcontract.time_update,
            tcontract.status
        );        
    }    

    function usdt_to_address_external(address _address, uint _amount) public onlyValidContract returns(bool result){
        require(usdt_to_address(_address, _amount));        
        return true;
    }

    function usdt_from_address_external(address _address, uint _amount) public onlyValidContract returns(bool result){
        require(usdt_from_address(_address, _amount));        
        return true;
    }

    function usdt_to_address(address _address, uint _amount) internal returns(bool result){
        require(_amount > 0);
        require(tether_token.balanceOf(address(this)) >= _amount);
        require(tether_token.transfer(_address, _amount));
        return true;
    }

    function usdt_from_address(address _address, uint _amount) internal returns(bool result){
        require(_amount > 0);
        require(tether_token.balanceOf(_address) >= _amount);
        require(tether_token.allowance(_address, address(this)) >= _amount);
        require(tether_token.transferFrom(_address, address(this), _amount));
        return true;
    }

    function get_user_balance_allowance(address _address) public view returns(uint balance) {
        return tether_token.allowance(_address, address(this));
    }

    function get_investor_balance(address _address) public view returns(uint balance) {
        if(investors[_address].time_update < 1) return 0;        
        return investors[_address].amount;        
    }

    function get_invest_bank() public view returns(uint) {
        return invest_bank;
    }

    function get_reward_bank() public view returns(uint) {
        return reward_bank;
    }        

    function reward_bank_update(uint _amount) public onlyValidContract returns(bool result){
        require(_amount > 0);
        if(rewards_not_update) {
            reward_bank = reward_bank.add(_amount);
        }else{
            reward_bank2 = reward_bank2.add(_amount);
        }
        
        return true; 
    }    

    event Invest(address _address, uint _amount);

    function invest_money_add(uint _amount) public returns(bool result) {
        require(_amount >= min_amount);
        require(usdt_from_address(msg.sender, _amount));        
        uint k = 0;
        if(investors[msg.sender].time_update > 0) k = 1;
        investors[msg.sender] = Investor(investors[msg.sender].amount.add(_amount), now);                
        invest_bank = invest_bank.add(_amount);
        emit Invest(msg.sender, _amount);                                        
        if(k < 1) inv.push(msg.sender);        
        return true;
    }    

    event Withdraw(address _address, uint _amount);

    function invest_money_withdraw(uint _amount) public returns(bool result) {        
        require(invest_bank >= _amount);
        require(investors[msg.sender].amount >= _amount);                
        require(usdt_to_address(msg.sender, _amount));        

        emit Withdraw(msg.sender, _amount);                                
        investors[msg.sender].amount = investors[msg.sender].amount.sub(_amount);        
        invest_bank = invest_bank.sub(_amount);
        return true;
    }

    event Reward_continue(uint time_update);
    event Reward_finish(uint time_finish);

    function inv_length() public view returns (uint result) {
        return inv.length;
    }

    function invest_rewards_update() public onlyOwner returns(uint result) {

        require (invest_bank > 0);
        require (reward_bank > 0);   

        rewards_not_update = false;

        address a;
        uint r;
        uint k = 0;
        uint u1;
        uint u2;        

        u1 = user_id_last;
        u2 = user_id_last + users_step;
        if(u2 >= inv.length) {
            u2 = inv.length;
            k = 1;
        } 

        for (uint i = u1; i < u2; i++) {
            a = inv[i];
            r = reward_bank.mul(investors[a].amount).div(invest_bank);            
            if(r == 0) continue;            
            investors[a].amount = investors[a].amount.add(r);
            invest_bank = invest_bank.add(r);
        }

        user_id_last = u2;
        
        if(k == 0) {
            emit Reward_continue(now);  
            return 1;
        } 

        emit Reward_finish(now);  

        rewards_not_update = true;                                 
        reward_bank = reward_bank2;
        reward_bank2 = 0;        
        user_id_last = 0;

        return 2;    
    }        

    function investor_info(address _address) public view 
            returns (
                uint event_id,                    
                uint time_start) {
        Investor memory investor = investors[_address];
        return(
            investor.amount,
            investor.time_update
        );        
    }

    modifier onlyOwner() {
        if (msg.sender != owner) {
            revert();
        }
        _;
    }   

    modifier onlyValidContract(){
        require (contracts[msg.sender].status);
        _;
    } 

}

library SafeMath {

    function mul(uint a, uint b) internal pure returns (uint) {
      uint c = a * b;
      assert(a == 0 || c / a == b);
      return c;
    }

    function div(uint a, uint b) internal pure returns (uint) {
      
      uint c = a / b;      
      return c;
    }

    function sub(uint a, uint b) internal pure returns (uint) {
      assert(b <= a);
      return a - b;
    }

    function add(uint a, uint b) internal pure returns (uint) {
      uint c = a + b;
      require(c >= a);
      return c;
    }

}