pragma solidity ^0.5.16;

library SmallNumbers {

	function fromInt (uint x) internal pure returns (uint) {
	      return x << 64;	    
  	}

  	function toInt (uint x) internal pure returns (uint) {    
      return x >> 64;
    }

    function smul (uint x, uint y) internal pure returns (uint) {      
      return x * y >> 64;
    }

    function sdiv (uint x, uint y) internal pure returns (uint) {      
      require (y >0);
      return (x << 64)/y;
    }  

    function ssqrtu (uint x) internal pure returns (uint128) {
        
      if (x == 0) return 0;
      else {
        uint256 xx = x;
        uint256 r = 1;
        if (xx >= 0x100000000000000000000000000000000) { xx >>= 128; r <<= 64; }
        if (xx >= 0x10000000000000000) { xx >>= 64; r <<= 32; }
        if (xx >= 0x100000000) { xx >>= 32; r <<= 16; }
        if (xx >= 0x10000) { xx >>= 16; r <<= 8; }
        if (xx >= 0x100) { xx >>= 8; r <<= 4; }
        if (xx >= 0x10) { xx >>= 4; r <<= 2; }
        if (xx >= 0x8) { r <<= 1; }
        r = (r + x / r) >> 1;
        r = (r + x / r) >> 1;
        r = (r + x / r) >> 1;
        r = (r + x / r) >> 1;
        r = (r + x / r) >> 1;
        r = (r + x / r) >> 1;
        r = (r + x / r) >> 1; // Seven iterations should be enough
        uint256 r1 = x / r;
        return uint128 (r < r1 ? r : r1);
      }
    }  
  
}