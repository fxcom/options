pragma solidity ^0.5.16;

import "./math.sol";

contract CND is Math1{

	uint public k1;
	uint public k2;
	uint public k3;
	uint public k4;
	uint public k5;

	uint[] public A = [0,319381530,356563782,1781477937,1821255978,1330274429];                        		

	constructor() public{        		
		
	}		

	function get_x(uint x) public returns (uint w){
		require(x>0);
		uint l = x;
		
		k1 = 1000*1000000000/(100000000+231641900*l);
		k2 = k1*k1/1000;
		k3 = k2*k1/1000;
		k4 = k3*k1/1000;
		k5 = k4*k1/1000;

		uint s = uint(271828)/uint(100000);
		uint val = uint(628318530)/uint(100000000);
		uint sq = Math1.sqrt(val);

		uint poly = (A[1]*k1+A[3]*k3+A[5]*k5-A[2]*k2-A[4]*k4)/1000000000;
		w = 1000*(1 - s**(-l*l/2)*poly/sq);
		return w;
	}

}