pragma solidity ^0.5.10;

contract Math1 {

	constructor() public{        		
		
	}	

	function sqrt(uint x) pure public returns (uint y) {
	  uint z = (x + 1) / 2;
	  y = x;
	  while (z < y) {
	      y = z;
	      z = (x / z + z) / 2;
    	}
	}

}