pragma solidity ^0.5.16;

library SmallNumbers {

	function fromInt (uint x) internal pure returns (uint) {
	      return x << 64;	    
  	}

  	function toInt (uint x) internal pure returns (uint) {    
      return x >> 64;
    }

    function mul (uint x, uint y) internal pure returns (uint) {      
      return x * y >> 64;
    }

    function div (uint x, uint y) internal pure returns (uint) {      
      require (y >0);
      return (x << 64)/y;
    }
  
    
  
}