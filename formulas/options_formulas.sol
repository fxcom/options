pragma solidity ^0.5.16;

//import "./cnd.sol";
import {SmallNumbers} from "./small_numbers2.sol";  

contract Formulas {

	using SmallNumbers for int;  

	int8 constant private PRECISION = 32;  // fractional bits	

	constructor() public{        		
		
	}	
	
	//X - sNumberPresition
	//hv - sNumberPresition
	//option_price - sNumberPresition
	function binary_call(int X, int _hv) internal pure returns(int option_price){		
		option_price = (X*SmallNumbers.normsDist(_hv/2))>>PRECISION;		
		return option_price;
	}
	
	//X - sNumberPresition
	//hv - sNumberPresition
	//option_price - sNumberPresition
	function binary_put(int X, int _hv) internal pure returns(int option_price){		
		option_price = (X*(1<<PRECISION-SmallNumbers.normsDist(_hv/2)))>>PRECISION;		
		return option_price;
	}

	//X - sNumberPresition
	//hv - sNumberPresition
	//option_price - sNumberPresition
	function binary_callput(int X, int _hv) public pure returns(int option_price){		
		int k = binary_call(X, _hv); // sNumberPresition
		int m = binary_put(X, _hv); // sNumberPresition
		option_price = k;
		if(m < k) option_price = m;
		return option_price;
	}
	/*
	function ln(uint b) pure public returns (uint){
        uint up = 256;
        uint down = 0;
        uint attempt = (up+down)/2;
        while (up>down+1){
            if(b>=(2**attempt)){
                down=attempt;
            }else{
                up=attempt;
            }
            attempt=(up+down)/2;
        }
        return attempt;
    }
	*/

	//S, K, _hv, d1 - sNumberPresition
    function d1_call(int S, int K, int _hv) pure internal returns(int d1){

    	require(S<=K);    	
		d1 = _hv/2 + int((SmallNumbers.ln(uint(S))-SmallNumbers.ln(uint(K))))<<PRECISION/_hv;
		return d1;
	}

	//S, K, _hv, d2 - sNumberPresition

	function d2_call(int S, int K, int _hv) pure internal returns(int d2){
		require(S<=K);    	
		int d1 = d1_call(S,K,_hv);
		d2;
		if(d1 < _hv) {
			d2 = _hv-d1;
		}else{
			d2 = d1 - _hv;
		}
		
		return d2;
	}

	//S, K, _hv, d1 - sNumberPresition
	function d1_put(int S, int K, int _hv) pure internal returns(int d1){

    	require(S>K);    	
		d1 = int(SmallNumbers.ln(uint(S/K)))/_hv + _hv/2;
		return d1;
	}

	//S, K, _hv, d2 - sNumberPresition
	function d2_put(int S, int K, int _hv) pure internal returns(int d2){
		require(S>K);    	
		int d1 = d1_put(S,K,_hv);
		if(d1 < _hv) {
			d2 = _hv-d1;
		}else{
			d2 = d1 - _hv;
		}
		
		return d2;
	}

	//S, K, _hv, X, option_price - sNumberPresition
	function option_touch_call(int S, int K, int _hv, int X) public pure returns(int option_price){
		int d1 = d1_call(S, K, _hv);
		int d2 = d2_call(S, K, _hv);
		option_price = X*(1<<PRECISION-SmallNumbers.normsDist(d1)- K/S+K*SmallNumbers.normsDist(d2)/S);				
		return option_price;
	}

	//S, K, _hv, X, option_price - sNumberPresition
	function option_touch_put(int S, int K, int _hv, int X) public pure returns(int option_price){
		int d1 = d1_put(S, K, _hv);
		int d2 = d2_put(S, K, _hv);
		option_price = X*SmallNumbers.normsDist(d1) - X*K*SmallNumbers.normsDist(d2)/S;		
		return option_price;
	}

	//S, K, _hv, X, option_price - sNumberPresition
	function option_no_touch_call(int S, int K, int _hv, int X) public pure returns(int option_price){
		int d1 = d1_call(S, K, _hv);
		int d2 = d2_call(S, K, _hv);
		option_price = SmallNumbers.normsDist(d1)+X*K/S-(X*K*SmallNumbers.normsDist(d2)/S)>>PRECISION;		
		return option_price;
	}

	//S, K, _hv, X, option_price - sNumberPresition
	function option_no_touch_put(int S, int K, int _hv, int X) public pure returns(int option_price){
		int d1 = d1_call(S, K, _hv);
		int d2 = d2_call(S, K, _hv);
		option_price = X*(1<<PRECISION-(1<<PRECISION-SmallNumbers.normsDist(d1))+K*(1<<PRECISION-SmallNumbers.normsDist(d2)))/S;		
		return option_price;
	}

	//S, KH, KL, _hv, X, option_price - sNumberPresition	
	function option_double_no_touch(int S, int KH, int KL, int _hv, int X) public pure returns(int option_price){
		int d2H = d2_call(S, KH, _hv);
		int d2L = d2_call(S, KL, _hv);
		option_price = X*(SmallNumbers.normsDist(d2H)-SmallNumbers.normsDist(d2L))>>PRECISION;		
		return option_price;
	}

	//S, KH, KL, _hv, X, option_price - sNumberPresition
	function option_double_touch(int S, int KH, int KL, int _hv, int X) public pure returns(int option_price){		
		option_price = option_double_no_touch(S, KH, KL, _hv, X) 
		  + option_touch_call(S, KH, _hv, X)
		  + option_touch_put(S, KL, _hv, X) - X;
		return option_price;
	}

	//S, K, _hv, X, option_price - sNumberPresition
	function option_american_call(int S, int K, int _hv, int X) public pure returns(int option_price){		
		int d1 = d1_call(S, K, _hv);
		int d2 = d2_call(S, K, _hv);
		option_price = X*(S*SmallNumbers.normsDist(d1) - K*SmallNumbers.normsDist(d2))>>PRECISION;
		return option_price;
	}

	//S, K, _hv, X, option_price - sNumberPresition
	function option_american_put(int S, int K, int _hv, int X) public pure returns(int option_price){		
		int d1 = d1_put(S, K, _hv);
		int d2 = d2_put(S, K, _hv);
		option_price = X*(S*SmallNumbers.normsDist(d1) - K*SmallNumbers.normsDist(d2))>>PRECISION;
		return option_price;
	}
	
	//_r1, _r2 - sNumberPresition
	function adj_coef(int _r1, int _r2) pure public returns (int coef){
		int k = 0;
		int l = (int(5)<<PRECISION)/10;
		if(_r1 > l) {
			k = l;
		}
		coef = int(uint(uint(uint(k + int(10)<<PRECISION)**uint(4*_r2))<<PRECISION)/(uint(10)**uint(4*_r2)));
		return coef;
	}

}