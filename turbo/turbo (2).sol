pragma solidity ^0.5.10;        

// ----------------------------------------------------------------------------------------------
// Developer Nechesov Andrey: Facebook.com/Nechesov      
// ----------------------------------------------------------------------------------------------

contract TetherToken {
    function balanceOf(address who) external view returns (uint256);    
    function transfer(address to, uint256 value) public returns (bool);
    function transferFrom(address from, address to, uint256 value) public returns (bool);
    function allowance(address _owner, address _spender) external view returns (uint256);
}

contract Staking {      

    using SafeMath for uint;

    TetherToken tether_token = TetherToken(0x4137ff8e2c9173a6253d38528608592b58126a2250);

    //type Long = 1, Short = 0;
    //funding rate
    uint public frx10;
    //number of turbo in 1 BTC
    uint[] public ratio = [1,10,100,1000,10000];
    uint[] public leverage = [1,5,10,50,100];
    uint[] public protectionx100 = [0,10,15,20];

    constructor() public {        
        frx10 = 5;
    } 

    function fr(uint _frx10) public onlyOwner returns (bool) {
        frx10 = _frx10;
    }

    function fr(uint _frx10) public onlyOwner returns (bool) {
        frx10 = _frx10;
    }
    // CALL: _type =1, SHORT: _type = 0;
    function get_strike_price(uint _type; uint _current_price, uint _leverage) public onlyOwner returns (uint strike_price) {
        require(_type <= 1);
        uint r = 0;
        uint l = _leverage;
        uint s0 = _current_price;
        for (uint i = 0; i < leverage.length; i++){
            if(l = leverage[i]) r = 1;
        }
        require(r==1);        

        //type LONG
        if(_type == 1) strike_price = (l-1).mul(s_0).div(l);
        if(_type == 0) strike_price = (l+1).mul(s_0).div(l);

        return strike_price;

    }

    function get_barrier(uint _type, uint _current_price, uint _strike_price, uint _protectionx100) public onlyOwner returns (uint barrier) {
        
        require(_type <= 1);
        require(_current_price > 0);        
        require(_strike_price > 0);
        uint r = 0;
        uint p = _protectionx100;
        uint s0 = _current_price;
        uint k = _strike_price;

        for (uint i = 0; i < protectionx100.length; i++){
            if(p = protectionx100[i]) r = 1;
        }
        require(r==1);

        if(_type == 1) {
            require(s0>k);
            barrier = k+ (s0-k).mul(p);  
        }
        if(_type == 0) {
            require(s0<k);
            barrier = k+ (s0-k).mul(p);  
        }
        return barrier;
    }

    function investor_info(address _address) public view 
            returns (
                uint event_id,                    
                uint time_start) {
        Investor memory investor = investors[_address];
        return(
            investor.amount,
            investor.time_update
        );        
    }

    modifier onlyOwner() {
        if (msg.sender != owner) {
            revert();
        }
        _;
    }   

    modifier onlyValidContract(){
        require (contracts[msg.sender].status);
        _;
    } 

}

library SafeMath {

    function mul(uint a, uint b) internal pure returns (uint) {
      uint c = a * b;
      assert(a == 0 || c / a == b);
      return c;
    }

    function div(uint a, uint b) internal pure returns (uint) {
      
      uint c = a / b;      
      return c;
    }

    function sub(uint a, uint b) internal pure returns (uint) {
      assert(b <= a);
      return a - b;
    }

    function add(uint a, uint b) internal pure returns (uint) {
      uint c = a + b;
      require(c >= a);
      return c;
    }

}