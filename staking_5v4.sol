pragma solidity ^0.5.10;        

// ----------------------------------------------------------------------------------------------
// Developer Nechesov Andrey: Facebook.com/Nechesov      
// ----------------------------------------------------------------------------------------------

contract TetherToken {
    function balanceOf(address who) external view returns (uint256);    
    function transfer(address to, uint256 value) public returns (bool);
    function transferFrom(address from, address to, uint256 value) public returns (bool);
    function allowance(address _owner, address _spender) external view returns (uint256);
}

contract BlzToken {
    function balanceOf(address who) external view returns (uint256);    
    function transfer(address to, uint256 value) public returns (bool);
    function transferFrom(address from, address to, uint256 value) public returns (bool);
    function allowance(address _owner, address _spender) external view returns (uint256);
}

contract BlzStaking {
    function blz_to_address_external(address _address, uint _amount) public returns (bool);        
    function get_investor_time_update(address _address) public view returns(uint time_update);
}

contract BlzToken {
    function balanceOf(address who) external view returns (uint256);    
    function transfer(address to, uint256 value) public returns (bool);
    function transferFrom(address from, address to, uint256 value) public returns (bool);
    function allowance(address _owner, address _spender) external view returns (uint256);
}

contract BlxStaking {    
    function get_staking_balance(address _address) public view returns(uint time_update);
}

contract Staking {      

    using SafeMath for uint;
    
    //USDT-contract address: TF5JJucRfv3AUJKtqk6uxh6aozYD1TfvuP
    TetherToken tether_token = TetherToken(0x4137ff8e2c9173a6253d38528608592b58126a2250); 
    address public blz_token_address = 0x546C50d43c5ab46335029096Fa0FA83143304f33;
    BlzToken blz_token = BlzToken(0x546C50d43c5ab46335029096Fa0FA83143304f33);             
    BlzStaking blz_staking = BlzStaking(0x1d58E093C544F441718c19329dF54B7251aEE1A8); 
    BlxStaking blx_staking = BlxStaking(0x166E38Df9388b5Ef167DA0F3D8F4002b6412bA56); 

    struct TContract {                        
        uint time_update; 
        bool status;            
    }

    struct Investor {                        
        uint amount; 
        uint time_update;            
    }   

    uint public invest_bank; // total money from investors    

    uint public reward_bank; // reward bank for investors            
    uint public reward_bank2; // reward bank for investors when started reward update function            
    bool public rewards_not_update;     

    uint public reward_bank_digopt; // reward bank digital options
    uint public reward_bank_digopt2;    
    bool public rewards_digopt_not_update;

    uint public reward_bank_ameropt; // reward bank american options
    uint public reward_bank_ameropt2;
    bool public rewards_ameropt_not_update;  

    uint public reward_bank_turbosopt; // reward bank turbos options
    uint public reward_bank_turbosopt2;
    bool public rewards_turbosopt_not_update;
    
    address[] public inv;
    uint public user_id_last = 0;
    uint public user_id_last_digopt = 0;
    uint public user_id_last_ameropt = 0;
    uint public user_id_last_turbosopt = 0;
    uint public users_step = 100;   
    address public owner;        

    uint invest_bank_help = 0;
    uint invest_bank_help_digopt = 0;
    uint invest_bank_help_ameropt = 0;
    uint invest_bank_help_turbosopt = 0;
    uint public min_amount;  
    uint public min_blz_for_bonus;  

    uint public total_call_amount;         
    uint public total_put_amount;         

    mapping(address => Investor) investors;                 
    mapping(address => TContract) contracts; // trusted contracts                     

    mapping(uint => uint) size;// multiply by 100
    uint commission = 10;
    
    //address | weeks | money
    //mapping(address => mapping(uint => uint)) public balances;
    //address | weeks | array
    //mapping(address => uint[]) public iweeks;


    constructor() public{        
        reward_bank = 0;
        reward_bank2 = 0;
        invest_bank = 0;
        min_amount = 10**6;
        min_blz_for_bonus = 1;
        rewards_not_update = true;
        owner = msg.sender;   //set Owner address  
        contracts[msg.sender] = TContract(now, true);
        contracts[address(this)] = TContract(now, true);  

        size[0] = 1;      
        size[1] = 10;      
        size[2] = 100;      
        size[3] = 1000;      
    }

    function set_min_amount(uint _amount) public onlyOwner {
        min_amount = _amount;                
    }        

    function trusted_contracts_add(address _address) public onlyOwner {
        contracts[_address] = TContract(now, true);        
    }

    function trusted_contracts_delete(address _address) public onlyOwner {
        contracts[_address] = TContract(now, false);                
    }

    function trusted_contracts_info(address _address) public view 
            returns (
                uint time_update,                    
                bool status) {
       TContract memory tcontract = contracts[_address];
        return(
            tcontract.time_update,
            tcontract.status
        );        
    }    

    function add_usdt_to_address_with_blz_bonus(address _address, uint _amount) public onlyValidContract returns(bool result){                
        require(usdt_to_address(_address, _amount));        
        require(blz_token.balanceOf(_address) < min_blz_for_bonus);
        require(blz_token.balanceOf(blz_token_address)>=_amount);
        require(blz_staking.blz_to_address_external(_address, _amount));
        return true;
    }

    function usdt_to_address_external(address _address, uint _amount) public onlyValidContract returns(bool result){
        require(usdt_to_address(_address, _amount));        
        return true;
    }

    function usdt_from_address_external(address _address, uint _amount) public onlyValidContract returns(bool result){
        require(usdt_from_address(_address, _amount));        
        return true;
    }

    function usdt_to_address(address _address, uint _amount) internal returns(bool result){
        require(_amount > 0);
        require(tether_token.balanceOf(address(this)) >= _amount);
        require(tether_token.transfer(_address, _amount));
        return true;
    }

    function usdt_from_address(address _address, uint _amount) internal returns(bool result){
        require(_amount > 0);
        require(tether_token.balanceOf(_address) >= _amount);
        require(tether_token.allowance(_address, address(this)) >= _amount);
        require(tether_token.transferFrom(_address, address(this), _amount));
        return true;
    }

    function get_user_balance_allowance(address _address) public view returns(uint balance) {
        return tether_token.allowance(_address, address(this));
    }

    function get_investor_balance(address _address) public view returns(uint balance) {
        if(investors[_address].time_update < 1) return 0;        
        return investors[_address].amount;        
    }

    function get_invest_bank() public view returns(uint) {
        return invest_bank;
    }

    function get_reward_bank() public view returns(uint) {
        return reward_bank;
    }        

    function value_of_collaterial_for_call_update(uint _amount, uint sign) public onlyValidContract returns(uint result){
        if(sign == 0) {
            total_call_amount -= _amount;
        }else{
            total_call_amount += _amount;
        } 
        return total_call_amount;
    }

    function value_of_collaterial_for_put_update(uint _amount, uint sign) public onlyValidContract returns(uint result){
        if(sign == 0) {
            total_put_amount -= _amount;
        }else{
            total_put_amount += _amount;
        } 
        return total_put_amount;
    }

    function value_of_collaterial_total() public view returns(uint result){
        return total_put_amount + total_call_amount;
    }

    // 0 put, 1 call
    // r1*10;
    function get_r1(uint option_type) public view returns(uint r1){
        uint k;
        uint l;
        if(option_type == 0){
            k = total_put_amount;
        }else{
            k = total_call_amount;
        }
        l = value_of_collaterial_total();
        r1 = k*10/l;
        return r1;
    }

    // r2*100;
    function get_r2() public view returns(uint r2){
        r2 = value_of_collaterial_total()*100/invest_bank;
        return r2;
    }

    //adjustment coeff
    function adj_coef(uint option_type) public view returns(uint coef){
        uint k = 0;
        uint m = 4*get_r2();
        uint r1 = get_r1(option_type);
        if(r1 > 5){
            k = r1;
        }
        coef = k**m/1000;
        if(coef < 1) coef = 1;
        return coef;
    }
    
    function reward_bank_update(uint _amount) public onlyValidContract returns(bool result){
        require(_amount > 0);
        if(rewards_not_update) {
            reward_bank = reward_bank.add(_amount);
        }else{
            reward_bank2 = reward_bank2.add(_amount);
        }
        
        return true; 
    }    

    function reward_bank_digopt_update(uint _amount) public onlyValidContract returns(bool result){
        require(_amount > 0);        
        if(rewards_digopt_not_update) {
            reward_bank_digopt = reward_bank_digopt.add(_amount);
        }else{
            reward_bank_digopt2 = reward_bank_digopt2.add(_amount);
        }
        
        return true; 
    }

    function reward_bank_ameropt_update(uint _amount) public onlyValidContract returns(bool result){
        require(_amount > 0);        
        if(rewards_ameropt_not_update) {
            reward_bank_ameropt = reward_bank_ameropt.add(_amount);
        }else{
            reward_bank_ameropt2 = reward_bank_ameropt2.add(_amount);
        }
        
        return true; 
    }    

    function reward_bank_turbosopt_update(uint _amount) public onlyValidContract returns(bool result){
        require(_amount > 0);
        if(rewards_turbosopt_not_update) {
            reward_bank_turbosopt = reward_bank_turbosopt.add(_amount);
        }else{
            reward_bank_turbosopt2 = reward_bank_turbosopt2.add(_amount);
        }
        
        return true; 
    }    

    event Invest(address _address, uint _amount);

    function invest_money_add(uint _amount) public returns(bool result) {
        require(_amount >= min_amount);
        require(usdt_from_address(msg.sender, _amount));        
        uint k = 0;
        if(investors[msg.sender].time_update > 0) k = 1;
        investors[msg.sender] = Investor(investors[msg.sender].amount.add(_amount), now);                
        invest_bank = invest_bank.add(_amount);
        emit Invest(msg.sender, _amount);                                        
        if(k < 1) inv.push(msg.sender);        
        return true;
    }    

    event Withdraw(address _address, uint _amount);

    function invest_money_withdraw(uint _amount) public returns(bool result) {        
        require(invest_bank >= _amount);
        require(investors[msg.sender].amount >= _amount);                
        require(usdt_to_address(msg.sender, _amount));        

        emit Withdraw(msg.sender, _amount);                                
        investors[msg.sender].amount = investors[msg.sender].amount.sub(_amount);        
        invest_bank = invest_bank.sub(_amount);
        return true;
    }

    event Reward_continue(uint time_update);
    event Reward_finish(uint time_finish);

    function inv_length() public view returns (uint result) {
        return inv.length;
    }

    function invest_rewards_update() public onlyOwner returns(uint result) {

        require (invest_bank > 0);
        require (reward_bank > 0);   

        rewards_not_update = false;

        address a;
        uint r;
        uint k = 0;
        uint u1;
        uint u2;        

        u1 = user_id_last;
        u2 = user_id_last + users_step;
        if(u2 >= inv.length) {
            u2 = inv.length;
            k = 1;
        } 

        for (uint i = u1; i < u2; i++) {
            a = inv[i];
            r = reward_bank.mul(investors[a].amount).div(invest_bank);            
            if(r == 0) continue;            
            investors[a].amount = investors[a].amount.add(r);
            invest_bank_help += invest_bank.add(r);
        }

        user_id_last = u2;
        
        if(k == 0) {
            emit Reward_continue(now);  
            return 1;
        } 

        invest_bank = invest_bank_help;
        invest_bank_help = 0;

        emit Reward_finish(now);  

        rewards_not_update = true;                                 
        reward_bank = reward_bank2;
        reward_bank2 = 0;        
        user_id_last = 0;

        return 2;    
    }        
    //digital options rewards
    function invest_rewards_digopt_update() public onlyOwner returns(uint result) {

        require (invest_bank > 0);
        require (reward_bank_digopt > 0);  
        require(rewards_digopt_not_update); 
        require(rewards_turbosopt_not_update); 

        rewards_digopt_not_update = false;

        address a;
        uint r;
        uint k = 0;
        uint u1;
        uint u2;        
        uint pr;        

        u1 = user_id_last_digopt;
        u2 = user_id_last_digopt + users_step;
        if(u2 >= inv.length) {
            u2 = inv.length;
            k = 1;
        } 

        for (uint i = u1; i < u2; i++) {
            a = inv[i];           

            pr = 12;
            if((investors[a].time_update + 52*7*84600 > now)&&(blx_staking.get_staking_balance(a) >= investors[a].amount)) pr = 18;
            if(investors[a].time_update + 52*7*84600 < now)&&(blx_staking.get_staking_balance(a) < investors[a].amount) pr = 24;
            if((investors[a].time_update + 52*7*84600 < now)&&(blx_staking.get_staking_balance(a) >= investors[a].amount)) pr = 36;

            r = reward_bank_digopt.mul(pr*investors[a].amount*commission).div(invest_bank*100*100);            
            if(r == 0) continue;            
            investors[a].amount = investors[a].amount.add(r);
            invest_bank_help += invest_bank.add(r);
        }

        user_id_last_digopt = u2;
        
        if(k == 0) {
            emit Reward_continue(now);  
            return 1;
        } 

        invest_bank = invest_bank_help;
        invest_bank_help = 0;

        emit Reward_finish(now);  

        rewards_not_update = true;                                 
        reward_bank_digopt = reward_bank_digopt2;
        reward_bank_digopt2 = 0;        
        user_id_last_ameropt = 0;

        return 2;    
    }        

    //american options rewards
    function invest_rewards_ameropt_update() public onlyOwner returns(uint result) {

        require (invest_bank > 0);
        require (reward_bank_ameropt > 0);  
        require(rewards_ameropt_not_update);
        require(rewards_turbosopt_not_update); 

        rewards_ameropt_not_update = false;

        address a;
        uint r;
        uint k = 0;
        uint u1;
        uint u2;        
        uint pr;        

        u1 = user_id_last_ameropt;
        u2 = user_id_last_ameropt + users_step;
        if(u2 >= inv.length) {
            u2 = inv.length;
            k = 1;
        } 

        for (uint i = u1; i < u2; i++) {
            a = inv[i];

            if(blz_staking.get_investor_time_update(a) > 0) {blz_duration = true;}else{blz_duration = false;}

            pr = 20;
            if((investors[a].time_update + 52*7*84600 > now)&&(blx_staking.get_staking_balance(a) >= investors[a].amount)) pr = 30;
            if(investors[a].time_update + 52*7*84600 < now)&&(blx_staking.get_staking_balance(a) < investors[a].amount) pr = 40;
            if((investors[a].time_update + 52*7*84600 < now)&&(blx_staking.get_staking_balance(a) >= investors[a].amount)) pr = 60;            

            r = reward_bank_ameropt.mul(pr*investors[a].amount*commission).div(invest_bank*100*100);            
            if(r == 0) continue;            
            investors[a].amount = investors[a].amount.add(r);
            invest_bank_help += invest_bank.add(r);
        }

        user_id_last_ameropt = u2;
        
        if(k == 0) {
            emit Reward_continue(now);  
            return 1;
        } 

        invest_bank = invest_bank_help;
        invest_bank_help = 0;

        emit Reward_finish(now);  

        rewards_not_update = true;                                 
        reward_bank_ameropt = reward_bank_ameropt2;
        reward_bank_ameropt2 = 0;        
        user_id_last_ameropt = 0;

        return 2;    
    }        

    //turbos options rewards
    function invest_rewards_turbos_update() public onlyOwner returns(uint result) {

        require (invest_bank > 0);
        require (reward_bank_turbosopt > 0);   

        rewards_turbosopt_not_update = false;

        address a;
        uint r;
        uint k = 0;
        uint u1;
        uint u2;        
        uint pr = 80;           

        u1 = user_id_last_turbosopt;
        u2 = user_id_last_turbosopt + users_step;
        if(u2 >= inv.length) {
            u2 = inv.length;
            k = 1;
        } 

        for (uint i = u1; i < u2; i++) {
            a = inv[i];                  

            r = reward_bank_turbosopt.mul(pr*investors[a].amount*commission).div(invest_bank*100*100);            
            if(r == 0) continue;            
            investors[a].amount = investors[a].amount.add(r);
            invest_bank_help += invest_bank.add(r);
        }

        user_id_last_turbosopt = u2;
        
        if(k == 0) {
            emit Reward_continue(now);  
            return 1;
        } 

        invest_bank = invest_bank_help;
        invest_bank_help = 0;

        emit Reward_finish(now);  

        rewards_not_update = true;                                 
        reward_bank_turbosopt = reward_bank_turbosopt2;
        reward_bank_turbosopt2 = 0;        
        user_id_last_turbosopt = 0;

        return 2;    
    }        

    function investor_info(address _address) public view 
            returns (
                uint event_id,                    
                uint time_start) {
        Investor memory investor = investors[_address];
        return(
            investor.amount,
            investor.time_update
        );        
    }

    modifier onlyOwner() {
        if (msg.sender != owner) {
            revert();
        }
        _;
    }   

    modifier onlyValidContract(){
        require (contracts[msg.sender].status);
        _;
    } 

}

library SafeMath {

    function mul(uint a, uint b) internal pure returns (uint) {
      uint c = a * b;
      assert(a == 0 || c / a == b);
      return c;
    }

    function div(uint a, uint b) internal pure returns (uint) {
      
      uint c = a / b;      
      return c;
    }

    function sub(uint a, uint b) internal pure returns (uint) {
      assert(b <= a);
      return a - b;
    }

    function add(uint a, uint b) internal pure returns (uint) {
      uint c = a + b;
      require(c >= a);
      return c;
    }

}