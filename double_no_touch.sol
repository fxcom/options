pragma solidity ^0.5.16;        

// ----------------------------------------------------------------------------------------------
// Developer Nechesov Andrey: Facebook.com/Nechesov      
// ----------------------------------------------------------------------------------------------

import "./formulas/options_formulas.sol";

contract Staking {
    function usdt_to_address_external(address _address, uint _amount) public returns (bool);    
    function usdt_from_address_external(address _address, uint _amount) public returns (bool);
    function get_user_balance_allowance(address _address) public view returns(uint);
    function reward_bank_update(uint _amount) public returns(bool);    
    function adj_coef(uint option_type) public returns(uint coef);  
}

contract DNT is Formulas {  

    using SafeMath for uint;  

    struct TOracle {                        
        uint time_update; 
        bool status;            
    }     

    struct Bet {
        uint bet_id;        
        address user_address;                
        uint amount;         
        uint hv;  
        uint option_price;           
        uint kl;
        uint kh;        
        uint time_open;         
        uint time_close; 
        uint price_open;               
        bool close;                
    }   
    
    event Bet_new(uint bet_id);
    event Bet_close(uint bet_id, bool close);

    //USDT-contract address: TNpP3csZPRPL39wS6s75fj1PBsrzStMbNj
    Staking staking = Staking(0x418cee46a5674902a704c432c06a58c805e387ee92); 

    mapping(address => TOracle) oracles; // trusted oracles  

    address public owner;                   
    uint public decimal; 
    uint public commission; 
    uint public bet_reward_procent; //     

    uint public mcl = 10**25; // max capital lose                 
    

    mapping (uint => Bet) public bets;         
    uint public num_bets;                                       

    Bet public bet;   

    constructor() public{        
        owner = msg.sender;   //set Owner address              
        decimal = 10;         
        bet_reward_procent = 2;            
    }

    function owner_change(address _owner_new) onlyOwner public {
        owner = _owner_new;
    }   

    function bet_reward_procent_change(uint _procent) onlyOwner public returns(bool result){
        bet_reward_procent = _procent;
        return true;
    }   

    function trusted_oracles_add(address _address) public onlyOwner {
        oracles[_address] = TOracle(now, true);        
    }

    function trusted_oracles_delete(address _address) public onlyOwner {
        oracles[_address] = TOracle(now, false);                
    }

    function trusted_oracles_info(address _address) public view 
            returns (
                uint time_update,                    
                bool status) {
       TOracle memory toracle = oracles[_address];
        return(
            toracle.time_update,
            toracle.status
        );        
    }

    function mcl_change(uint _mcl) onlyOwner public {
        mcl = _mcl;
    } 

    function commission_change(uint _commission) onlyOwner public {
        require(_commission > 0);
        require(_commission < 100);

        commission = _commission;
    }  

    function bet_add(uint X, uint _hv, uint _time_open, uint _time_close, uint _price_open, uint _kl, uint _kh) public returns (bool result){        

        //uint hv = HV.get_hv();
        uint option_price = uint(Formulas.option_double_no_touch(int(_price_open), int(_kl), int(_kh), int(_hv), int(X)));

        uint k = staking.adj_coef(0);
        if(k < staking.adj_coef(1)) k = staking.adj_coef(1);

        option_price = option_price * k;     
        
        require (staking.usdt_from_address_external(msg.sender, option_price));
        
        bet = Bet(num_bets, msg.sender, X, _hv, option_price, _kl, _kh, _time_open, _time_close, _price_open, false);            
        bets[num_bets] = bet;     
        num_bets++;  

        emit Bet_new(num_bets);                         
        
        result = true;    
        return result;
    }  

    function bet_pay(uint _bet_id) private returns (bool result){            
        
        Bet memory b = bets[_bet_id];        
        staking.usdt_to_address_external(b.user_address, b.amount);
        result = true;
        return result;
    } 

    function bet_close(uint _bet_id, uint _price_min, uint _price_max) public returns (bool result){        

        Bet memory b = bets[_bet_id];                          

        require (!b.close);                    
        require (b.time_close < 1);

        if((_price_min < b.kl)&&(_price_max > b.kh)){
            if(!bet_pay(_bet_id)) return false;                
            emit Bet_close(_bet_id, true);                   
            bets[_bet_id].close = true;        
            bets[_bet_id].time_close = now;        
            return true;
        }else{
            bets[_bet_id].close = true;        
            emit Bet_close(_bet_id, false);                   
        }        

        return true;
        
    }    

    modifier onlyOwner() {
        if (msg.sender != owner) {
            revert();
        }
        _;
    }

    modifier onlyOracle() {
        require (oracles[msg.sender].status);
        _;
    }
    

}

library SafeMath {

    function mul(uint a, uint b) internal pure returns (uint) {
      uint c = a * b;
      assert(a == 0 || c / a == b);
      return c;
    }

    function div(uint a, uint b) internal pure returns (uint) {
      
      uint c = a / b;      
      return c;
    }

    function sub(uint a, uint b) internal pure returns (uint) {
      assert(b <= a);
      return a - b;
    }

    function add(uint a, uint b) internal pure returns (uint) {
      uint c = a + b;
      require(c >= a);
      return c;
    }

}