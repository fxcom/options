pragma solidity ^0.5.10;        

// ----------------------------------------------------------------------------------------------
// Developer Nechesov Andrey: Facebook.com/Nechesov      
// ----------------------------------------------------------------------------------------------

contract TetherToken {
    function balanceOf(address who) external view returns (uint256);    
    function transfer(address to, uint256 value) public returns (bool);
    function transferFrom(address from, address to, uint256 value) public returns (bool);
    function allowance(address _owner, address _spender) external view returns (uint256);
}

contract Staking {  

    TetherToken tether_token = TetherToken(0x41c69c7aedf15e83a216193cfc0588a658c91c1e36aa0e35cf);        

    event Reward_finish(uint time_finish);

    struct Investor {                        
        uint amount; 
        uint time_update;            
    }   

    uint public invest_bank = 0; // total money from investors
    uint public reward_bank = 0; // reward bank for investors        
    uint public time_suspend = 24*3600;
    bool public invest_withdraw = false;
    
    address[] public inv;
    uint user_id_last = 0;
    uint users_step = 100;   
    address public owner;                  

    mapping(address => Investor) investors;                 
    mapping(address => bool) investors_exist; 

    constructor() public{        
        owner = msg.sender;   //set Owner address              
    }    

    function reward_bank_update(uint _amount) public onlyOwner {
        require(_amount > 0);
        reward_bank += _amount; 
    }

    function invest_money_add(address _address, uint _amount) public onlyOwner returns(bool result) {
        require(_amount > 0);
        require(tether_token.balanceOf(_address) >= _amount);
        require(tether_token.allowance(_address, address(this)) >= _amount);
        tether_token.transferFrom(_address, address(this), _amount);
        investors[_address] = Investor(investors[_address].amount + _amount, now);                        
        if(investors_exist[_address]){return true;}
        investors_exist[_address] = true;            
        inv.push(_address);
        invest_bank += _amount;
        return true;
    }

    function invest_money_withdraw(address _address, uint _amount) public returns(bool result) {

        require(_amount > 0);
        require(invest_withdraw);
        //require(address(this).balance >= _amount);            
        //require(tether_token.allowance(msg.sender, this) >= _amount);
        require(investors[_address].amount >= _amount);
        require(investors[_address].time_update + time_suspend <= now);
        
        require(tether_token.balanceOf(address(this)) >= _amount);
        tether_token.transfer(_address, _amount);
        invest_bank = invest_bank - _amount;
        return true;
    }

    function invest_rewards_update() public onlyOwner returns(uint result) {

        require (invest_bank > 0);
        require (reward_bank > 0);   

        invest_withdraw = false;

        address a;
        uint r;
        uint k = 0;
        uint u1;
        uint u2;
        uint r_total = 0;

        u1 = user_id_last;
        u2 = user_id_last + users_step;
        if(u2 >= inv.length) {
            u2 = inv.length;
            k = 1;
        } 

        for (uint i = u1; i < u2; i++) {
            a = inv[i];
            r = reward_bank*investors[a].amount/invest_bank;
            if(r == 0) continue;
            r_total = r_total + r;
            investors[a].amount = investors[a].amount + r;
        }

        user_id_last = u2+1;
        
        if(k == 0) return 1;

        emit Reward_finish(now);  

        invest_withdraw = true;                         
        invest_bank += reward_bank;
        reward_bank = 0;
        user_id_last = 0;

        return 2;    
    }    

    function time_suspend_change(uint _time_suspend) onlyOwner public {
        time_suspend = _time_suspend;
    }

    function investor_info(address _address) public view 
            returns (
                uint event_id,                    
                uint time_start) {
        Investor memory investor = investors[_address];
        return(
            investor.amount,
            investor.time_update
        );        
    }

    modifier onlyOwner() {
        if (msg.sender != owner) {
            revert();
        }
        _;
    }    

}