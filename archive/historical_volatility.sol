pragma solidity ^0.5.16;

import "./math.sol";

contract HV is Math1{

	uint public x1 = 0;
	uint public x2 = 0;
	uint public a0 = 0;
	uint public a1 = 0;
	uint public w = 1;
	uint public hv;

	//win total numbers of periods
	
	constructor(uint _win) public{        		
		
	}

	function init(uint _win) public returns (bool) {     	
		require(_win > 0);
		w = _win;   
		a0 = 1000*((uint(271828)/uint(100000))**(1/w)-1);
		a1 = 1000*((uint(271828)/uint(100000))**(1/w)-1);
		return true;		
    }

    function update(uint dp) public returns (uint) {
    	if(x1 < 1 && x2 < 1) {
    		x1 = dp;
    		x2 = dp*dp;
    	}else{
    		x1 = dp*a0/1000 + x1*a1/1000;
    		x2 = dp*dp*a0/1000 + x2*a1/1000;
    	}

    	hv = Math1.sqrt(x2-x1*x1);
    	return hv;        
    }

    function insert_hv(uint _hv) public returns (bool) {
    	hv = _hv;
    	return true;
    }  

    function get_hv() view public returns (uint) {
    	return hv;
    }  
    
}