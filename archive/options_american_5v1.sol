pragma solidity ^0.5.10;        

// ----------------------------------------------------------------------------------------------
// Developer Nechesov Andrey: Facebook.com/Nechesov      
// ----------------------------------------------------------------------------------------------

import "./formulas/options_formulas.sol";

contract Staking {
    function usdt_to_address_external(address _address, uint _amount) public returns (bool);    
    function usdt_from_address_external(address _address, uint _amount) public returns (bool);
    function get_user_balance_allowance(address _address) public view returns(uint);
    function reward_bank_update(uint _amount) public returns(bool);    
}

contract Options_american is Formulas {  

    using SafeMath for uint;  

    struct TOracle {                        
        uint time_update; 
        bool status;            
    }    

    struct Event {
        uint event_id; 
        string event_name; //BTCUSDT (for example)
        uint time_start;   //event time_start
        uint time_close;   //event time_close        
        uint time_gap;     //option duration time 5 min, 10 min, 60 min, 1 day and etc
        uint deviation_procent;
        uint min_bet;      
        uint max_bet;                                
    }      

    struct Bet {
        uint bet_id;
        address user_address;
        uint event_id;
        uint time_open; 
        uint time_close; 
        uint price_open;  //strike_price
        uint price_strike;
        uint price_close;
        uint out_id;      // CALL = 0; PUT = 1;     
        uint option_amount;  // 
        uint bet_premium; // option premium
        //uint bet_coef;    // coefs
        uint bet_status;  // 0 - open, 1 - close success, close fail
    }                     

    event Event_new(uint event_id);
    event Event_stop(uint event_id);
    event Bet_new(uint bet_id, uint event_id);
    event Bet_close(uint bet_id, uint event_id, bool status);
    event User_Close_Bet(uint bet_id, uint event_id);
    event Oracle_Close_Bet(uint bet_id, uint event_id);
    
    //USDT-contract address: TNpP3csZPRPL39wS6s75fj1PBsrzStMbNj
    Staking staking = Staking(0x418cee46a5674902a704c432c06a58c805e387ee92);  

    mapping(address => TOracle) oracles; // trusted oracles                       

    address public owner;                             

    Bet public bet;         
    uint public bet_reward_procent; // procent from all lost bets go to bankroll 

    uint[] public ds = [0,1179,2257,3139,3849,4332,4641,4821,4918,4965,4987];                        
    uint commission = 125;

    mapping (uint => Event) public events; 
    uint public num_events = 0;

    mapping (uint => Bet[]) public bets;         
    mapping (uint => uint) public num_bets;         

    constructor() public{        
        owner = msg.sender;   //set Owner address              
        bet_reward_procent = 2;     
        oracles[msg.sender] = TOracle(now, true);       
    }

    function formula_for_premium(uint _out_id, uint _price_open, 
                uint _price_strike, uint _deviation_procent, 
                uint _option_amount) public view returns(uint premium){

        uint k = 0;
        require(_price_open >0);
        require(_deviation_procent >0);

        if(_out_id == 0){
            require(_price_strike >= _price_open);
            k = (_price_strike-_price_open)*100/_price_open;
        }

        if(_out_id == 1){
            require(_price_strike <= _price_open);
            k = (_price_open-_price_strike)*100/_price_open;
        }

        require(k < _deviation_procent);

        uint n = k*10/_deviation_procent;

        require(n <= 10);

        premium = _option_amount*(ds[10]-ds[n])*commission/100;

        return premium;
    } 
    
    function owner_change(address _owner_new) onlyOwner public {
        owner = _owner_new;
    } 

    function bet_reward_procent_change(uint _procent) onlyOwner public returns(bool result){
        bet_reward_procent = _procent;
        return true;
    } 

    function trusted_oracles_add(address _address) public onlyOwner {
        oracles[_address] = TOracle(now, true);        
    }

    function trusted_oracles_delete(address _address) public onlyOwner {
        oracles[_address] = TOracle(now, false);                
    }

    function trusted_oracles_info(address _address) public view 
            returns (
                uint time_update,                    
                bool status) {
       TOracle memory toracle = oracles[_address];
        return(
            toracle.time_update,
            toracle.status
        );        
    }       

    function get_balance() public view returns(uint result){
        return staking.get_user_balance_allowance(msg.sender);   
    }
    
    function event_add(string memory _event_name, uint _time_gap, uint _deviation_procent, uint _min_bet, uint _max_bet) onlyOwner() public returns (uint event_id){
        require(bytes(_event_name).length > 0);        
        uint time_start = now;

        event_id = num_events;
        num_events++;
        
        events[event_id] = Event(event_id, _event_name, time_start, 0, _time_gap, _deviation_procent, _min_bet, _max_bet);            

        emit Event_new(event_id);                   
        return event_id;
    }                

    function bet_add(address _user_address, uint _event_id, uint _out_id, 
                     uint _option_amount, uint _time_open, uint _price_open, 
                     uint _price_strike) 
                onlyOwner public returns (bool){

        require (events[_event_id].time_start < now);
        require (events[_event_id].time_close < 1);        
        require(_time_open > 0);
        require(_price_open > 0);
        require(_price_strike > 0);                
        require (_option_amount >= events[_event_id].min_bet);
        require (_option_amount <= events[_event_id].max_bet);                 
        uint hv = HV.get_hv();
        uint bet_premium = Formulas.option_touch_call(_price_open, _price_strike, hv, _option_amount);
        require(staking.usdt_from_address_external(_user_address, bet_premium));                
        
        num_bets[_event_id]++;        
        
        bet = Bet(num_bets[_event_id]-1, _user_address, _event_id, _time_open, 0, _price_open, _price_strike, 0, _out_id, _option_amount, bet_premium, 0);            

        bets[_event_id].push(bet);     

        emit Bet_new(bet.bet_id, bet.event_id);                   
        
        return true;    
    }        

    function set_bet_price_close(uint _event_id, uint _bet_id, uint _price_close, uint _time_close) onlyOracle() public returns(bool result){

        Bet memory b = bets[_event_id][_bet_id];

        require(_price_close > 0);                        
        require(b.time_open > 0);                    
        require(b.price_open > 0);                    
        require(b.price_close == 0);                    
        require(b.time_open < b.time_close);   
        require(b.time_close == _time_close);                     
        require(b.time_open + events[_event_id].time_gap >= _time_close);

        bets[_event_id][_bet_id].price_close = _price_close;                         
        bets[_event_id][_bet_id].time_close = _time_close;                         

        return true;
        
    }                     

    function user_close_bet(uint _event_id, uint _bet_id) public returns (bool){

        Bet memory b = bets[_event_id][_bet_id];

        require(b.time_open > 0);        
        require(b.time_close == 0);        
        require(now <= b.time_open + events[_event_id].time_gap);                    

        b.time_close = now;
        emit User_Close_Bet(_bet_id,_event_id);

        return true;

    }

    function oracle_close_bet(uint _event_id, uint _bet_id) onlyOracle public returns (bool){

        Bet memory b = bets[_event_id][_bet_id];

        require(b.time_open > 0);        
        require(b.time_close == 0);        
        uint time_close = b.time_open + events[_event_id].time_gap;
        require(now >= time_close);                    
        
        b.time_close = time_close;
        emit Oracle_Close_Bet(_bet_id, _event_id);

        return true;

    }

    function bet_close(uint _event_id, uint _bet_id) onlyOwner public payable returns (bool result){

        Bet memory b = bets[_event_id][_bet_id];

        require(b.time_open > 0);            
        require(b.time_close > 0);            
        require(b.price_open > 0);
        require(b.price_strike > 0);
        require(b.price_close > 0); 
        require(b.bet_status == 0); 

        uint out_id = 2;  
        uint k;
        uint amount;

        if(b.price_strike < b.price_close) {
            out_id = 0;                
            k = b.price_close/b.price_strike;
        }   

        if(b.price_strike > b.price_close) {
            out_id = 1;                
            k = b.price_strike/b.price_close;
        }            

        if(b.out_id != out_id) {            
            staking.reward_bank_update(b.bet_premium*bet_reward_procent/100);
            b.bet_status = 2;
            emit Bet_close(_bet_id, _event_id, false);                   
            return true;
        }

        amount = k*b.option_amount;
        
        //require(bet_pay(_event_id,_bet_id));            

        require(staking.get_user_balance_allowance(address(this)) >= amount);                            
        require(staking.usdt_to_address_external(b.user_address, amount));        

        b.bet_status = 1;
        emit Bet_close(_bet_id, _event_id, true);                   

        return true;                                                                                                    

    }        

    function event_stop(uint _event_id) onlyOwner() public returns (bool result){
        events[_event_id].time_close = now;
        result = true;
        emit Event_stop(_event_id);                   
    }

    function event_info(uint _event_id) public view 
            returns (
                uint event_id,                    
                string memory event_name,
                uint time_start, 
                uint time_close, 
                uint time_gap,
                uint min_bet,
                uint max_bet
            ) {
        Event memory e = events[_event_id];                        

        return(                    
                e.event_id,
                e.event_name,
                e.time_start, 
                e.time_close, 
                e.time_gap, 
                e.min_bet,
                e.max_bet
        );
    }

    function bet_info(uint _event_id, uint _bet_id) public view 
            returns (
                uint bet_id,
                address user_address,
                uint event_id,
                uint time_open,                  
                uint price_open,
                uint price_close,
                uint out_id,
                uint option_amount,                                    
                uint bet_premium, 
                uint bet_status
            ) {
        Bet memory b = bets[_event_id][_bet_id];                        

        return(
                b.bet_id, 
                b.user_address, 
                b.event_id,
                b.time_open,                 
                b.price_open, 
                b.price_close,
                b.out_id, 
                b.option_amount,
                b.bet_premium,  
                b.bet_status
        );
    }                            

    modifier onlyOwner() {
        if (msg.sender != owner) {
            revert();
        }
        _;
    }

    modifier onlyOracle() {
        require (oracles[msg.sender].status);
        _;
    }

}

library SafeMath {

    function mul(uint a, uint b) internal pure returns (uint) {
      uint c = a * b;
      assert(a == 0 || c / a == b);
      return c;
    }

    function div(uint a, uint b) internal pure returns (uint) {
      
      uint c = a / b;      
      return c;
    }

    function sub(uint a, uint b) internal pure returns (uint) {
      assert(b <= a);
      return a - b;
    }

    function add(uint a, uint b) internal pure returns (uint) {
      uint c = a + b;
      require(c >= a);
      return c;
    }

}