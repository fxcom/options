pragma solidity ^0.5.10;        

// ----------------------------------------------------------------------------------------------
// Developer Nechesov Andrey: Facebook.com/Nechesov      
// ----------------------------------------------------------------------------------------------

import "./formulas/options_formulas.sol";

contract Staking {
    function usdt_to_address_external(address _address, uint _amount) public returns (bool);    
    function usdt_from_address_external(address _address, uint _amount) public returns (bool);
    function get_user_balance_allowance(address _address) public view returns(uint);
    function reward_bank_update(uint _amount) public returns(bool);    
}

contract Touch is Formulas {  

    using SafeMath for uint;  

    struct TOracle {                        
        uint time_update; 
        bool status;            
    } 

    struct Event {
        uint event_id; 
        string event_name;         
        uint type_id;   
        uint deviation_procent;             
        uint time_gap;
        uint time_start;
        uint time_close;  
        uint min_bet;
        uint max_bet;                   
    }      

    struct Bet {
        uint bet_id;
        address user_address;        
        uint event_id;       
        uint amount;         
        uint price_1;
        uint price_2;
        uint time_open; 
        uint time_close; 
        uint price_open;
        uint price_close;
        uint price_min;
        uint price_max;
        bool close;        
        uint coef;   
    }                     

    event Event_new(uint event_id);
    event Event_stop(uint event_id);
    event Bet_new(uint bet_id, uint event_id);
    event Bet_close(uint bet_id, uint event_id, bool close);

    //USDT-contract address: TNpP3csZPRPL39wS6s75fj1PBsrzStMbNj
    Staking staking = Staking(0x418cee46a5674902a704c432c06a58c805e387ee92); 

    mapping(address => TOracle) oracles; // trusted oracles                            

    address public owner;                   
    uint public decimal; 
    uint public commission; 
    uint public bet_reward_procent; //                                   

    Bet public bet;    

    uint[] public ds = [0,1179,2257,3139,3849,4332,4641,4821,4918,4965,4987];                        

    uint public mcl = 10**25; // max capital lose             

    mapping (uint => Event) public events; 
    uint public num_events = 0;

    mapping (uint => Bet[]) public bets;         
    mapping (uint => uint) public num_bets;         

    constructor() public{        
        owner = msg.sender;   //set Owner address              
        decimal = 10; 
        commission = 25;   
        bet_reward_procent = 2;            
    }
    
    function owner_change(address _owner_new) onlyOwner public {
        owner = _owner_new;
    }   

    function bet_reward_procent_change(uint _procent) onlyOwner public returns(bool result){
        bet_reward_procent = _procent;
        return true;
    }   

    function trusted_oracles_add(address _address) public onlyOwner {
        oracles[_address] = TOracle(now, true);        
    }

    function trusted_oracles_delete(address _address) public onlyOwner {
        oracles[_address] = TOracle(now, false);                
    }

    function trusted_oracles_info(address _address) public view 
            returns (
                uint time_update,                    
                bool status) {
       TOracle memory toracle = oracles[_address];
        return(
            toracle.time_update,
            toracle.status
        );        
    }                                        

    function mcl_change(uint _mcl) onlyOwner public {
        mcl = _mcl;
    }    

    function commission_change(uint _commission) onlyOwner public {
        require(_commission > 0);
        require(_commission < 100);

        commission = _commission;
    }  
    
    function event_add(string memory _event_name, uint _type_id, uint _deviation_procent, uint _time_gap, uint _min_bet, uint _max_bet) onlyOwner() public returns (uint event_id){
        
        require(bytes(_event_name).length > 0);
        require(_type_id > 0);        
        require(_deviation_procent > 0);        
        require (_time_gap > 0);
        require (_min_bet > 0);
        require (_max_bet > 0);
        

        uint time_start = now;

        event_id = num_events;
        num_events++;
        
        events[event_id] = Event(event_id, _event_name, _type_id, _deviation_procent, _time_gap, time_start, 0, _min_bet, _max_bet);            

        emit Event_new(event_id);                   
    }            

    function prob_1 (uint _event_id, uint _price_open, uint _price_x) view public returns (uint p){
        //price will be >= X, where X > price_open 

        Event memory e = events[_event_id];

        require(_price_open > 0);        
        require(_price_x > 0);          
        require(e.deviation_procent > 0);      
        require(_price_open < _price_x);

        uint s = 0;
        uint s1 = 0;
        
        s = _price_open*e.deviation_procent/100;  
        s1 = _price_x - _price_open;          

        uint k1 = 0;
        
        if(s1 >= s){ k1 = 10;
        }else{
            k1 = (s1/s)/10;
        }        

        p = (5000-ds[k1])/100;

    }

    function prob_2 (uint _event_id, uint _price_open, uint _price_x) view public returns (uint p){
        //price will be <= X, where X < price_open 
        require(_price_x < _price_open);
        p = prob_1(_event_id, _price_open, _price_open+(_price_open-_price_x));      
    }

    function prob_to_coef (uint p) view public returns (uint coef){
        require(p > 0);        
        coef = 100/p*(100-commission)/100;
        if(coef < 100) coef = 100;        
    }

    function coef_get (uint _event_id, uint _price_open, uint _price_1, uint _price_2) view public returns (uint coef){

        Event memory e = events[_event_id];

        require(_price_open > 0);                        
        require(e.deviation_procent > 0);   

        uint p = 0; 

        if(e.type_id == 1) {
            //Price to be X amount during bet        
            p = prob_1(_event_id, _price_open, _price_1);
            coef = prob_to_coef(p);
            return coef;
        }

        if(e.type_id == 2) {
            //Price to not be X amount during bet        
            p = prob_1(_event_id, _price_open, _price_1);
            coef = prob_to_coef(p);
            return coef;
        }

        if(e.type_id == 3) {
            //Price to be always above min price
            p = 100-prob_2(_event_id, _price_open, _price_1);
            coef = prob_to_coef(p);
            return coef;
        }

        if(e.type_id == 4) {
            //Price to be always below max price
            p = 100-prob_1(_event_id, _price_open, _price_2);
            coef = prob_to_coef(p);
            return coef;
        }

        if(e.type_id == 5) {
            //Price to be always between min price and max price    
            uint p1 = prob_1(_event_id, _price_open, _price_1);
            uint p2 = prob_2(_event_id, _price_open, _price_2);
            p = 100-(p1+p2);
            coef = prob_to_coef(p);
            return coef;
        }

    }                    

    function bet_add(uint _event_id, uint _amount, uint _cur_price, uint _price_1, uint _price_2) public returns (bool result){

        require (events[_event_id].time_start < now);
        require (events[_event_id].time_close < 1);                

        uint hv = HV.get_hv();
        uint option_price = Formulas.option_double_touch(_cur_price, _price_1, _price_2, hv, _amount, 1);    
        require (option_price >= events[_event_id].min_bet);
        require (option_price <= events[_event_id].max_bet);    
        require (staking.usdt_from_address_external(msg.sender, option_price));                

        uint time_open = now;
        uint time_close = time_open + events[_event_id].time_gap;
        num_bets[_event_id]++;
        bet = Bet(num_bets[_event_id]-1, msg.sender, _event_id, option_price, _price_1, _price_2, time_open, time_close, 0, 0, 0, 0, false, 0);            

        bets[_event_id].push(bet);     
        
        result = true;    
    }   

    function set_bet_oracle_params(uint _event_id, uint _bet_id, uint _price_open, uint _time_open, uint _price_close, uint _time_close, uint _price_min, uint _price_max) onlyOracle() public{

        Bet memory b = bets[_event_id][_bet_id];

        require (!b.close);            
        require (b.time_open == _time_open);            
        require (b.time_close == _time_close);            
        require (_price_open > 0);                        
        require (_price_close > 0);                       
        require (_price_min > 0);                        
        require (_price_max > 0);                         

        bets[_event_id][_bet_id].price_open = _price_open;
        bets[_event_id][_bet_id].coef = coef_get(_event_id, _price_open, b.price_1, b.price_2);
        bets[_event_id][_bet_id].price_close = _price_close;
        bets[_event_id][_bet_id].price_min = _price_max;
        bets[_event_id][_bet_id].price_max = _price_max;

        bet_close(_event_id,_bet_id);                         

    }        

    function bet_pay(uint _event_id, uint _bet_id) private returns (bool result){            
        
        Bet memory b = bets[_event_id][_bet_id];        
        staking.usdt_to_address_external(b.user_address, b.amount.mul(b.coef).div(100));
        result = true;
    }

    function bet_close_manual(uint _event_id, uint _bet_id) public onlyOwner returns (bool result){
        require(bet_close(_event_id,_bet_id));
        return true;
    }

    function bet_close(uint _event_id, uint _bet_id) internal returns (bool result){        

        Bet memory b = bets[_event_id][_bet_id];                          

        require (!b.close);            
        require (b.time_open > 0);
        require (b.time_close > 0);
        require (b.price_open > 0);
        require (b.price_close > 0);
        require (b.price_min > 0);
        require (b.price_max > 0);
        require (b.coef > 0);
        
        if(!bet_pay(_event_id,_bet_id)) return false;            
        bets[_event_id][_bet_id].close = true;        
        emit Bet_close(_bet_id, _event_id, true);                   

        return true;
        
    }    

    function event_info(uint _event_id) public view 
            returns (
                uint event_id,                    
                uint type_id, 
                uint deviation_procent, 
                uint time_gap,
                uint time_start,
                uint time_close,
                uint min_bet,                                    
                uint max_bet
            ) {
        Event memory e = events[_event_id];                        

        return(                    
                e.event_id,
                e.type_id, 
                e.deviation_procent, 
                e.time_gap, 
                e.time_start,
                e.time_close, 
                e.min_bet, 
                e.max_bet
        );
    }

    function bet_info(uint _event_id, uint _bet_id) public view 
            returns (
                uint bet_id,
                address user_address,
                uint event_id,
                uint amount,                 
                uint price_1,
                uint price_2,                
                uint price_min,
                uint price_max,
                bool close,
                uint coef
            ) {
        Bet memory b = bets[_event_id][_bet_id];                        

        return(
                b.bet_id, 
                b.user_address, 
                b.event_id,
                b.amount,                 
                b.price_1, 
                b.price_2,                
                b.price_min,
                b.price_max,
                b.close,
                b.coef
        );
    }        

    function event_stop(uint _event_id) onlyOwner() public returns (bool result){
        events[_event_id].time_close = now;
        result = true;
        emit Event_stop(_event_id);                   
    }                

    modifier onlyOwner() {
        if (msg.sender != owner) {
            revert();
        }
        _;
    }

    modifier onlyOracle() {
        require (oracles[msg.sender].status);
        _;
    }

}

library SafeMath {

    function mul(uint a, uint b) internal pure returns (uint) {
      uint c = a * b;
      assert(a == 0 || c / a == b);
      return c;
    }

    function div(uint a, uint b) internal pure returns (uint) {
      
      uint c = a / b;      
      return c;
    }

    function sub(uint a, uint b) internal pure returns (uint) {
      assert(b <= a);
      return a - b;
    }

    function add(uint a, uint b) internal pure returns (uint) {
      uint c = a + b;
      require(c >= a);
      return c;
    }

}