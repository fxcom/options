pragma solidity ^0.5.10;        

// ----------------------------------------------------------------------------------------------
// Developer Nechesov Andrey: Facebook.com/Nechesov      
// ----------------------------------------------------------------------------------------------
/*
contract TetherToken {
    function balanceOf(address who) external view returns (uint256);    
    function transfer(address to, uint256 value) public returns (bool);
    function transferFrom(address from, address to, uint256 value) public returns (bool);
    function allowance(address _owner, address _spender) external view returns (uint256);
}
*/

contract Staking {
    function usdt_to_address_external(address _address, uint _amount) public returns (bool);    
    function usdt_from_address_external(address _address, uint _amount) public returns (bool);
    function get_user_balance_allowance(address _address) public view returns(uint);
    function reward_bank_update(uint _amount) public returns(bool);    
}

contract BlzToken {
    function balanceOf(address who) external view returns (uint256);    
    function transfer(address to, uint256 value) public returns (bool);
    function transferFrom(address from, address to, uint256 value) public returns (bool);
    function allowance(address _owner, address _spender) external view returns (uint256);
}

contract Blz_staking {      

    using SafeMath for uint;

    //BLZ-contract address: TXsyLcSS4qqr66PnAmQsxWyHtBDbAVQ1e2
    BlzToken blz_token = BlzToken(0x41F055484082E86213745D916462871E5EE0CD994A);    

    struct TContract {                        
        uint time_update; 
        bool status;            
    }    

    struct Investor {                        
        uint amount; 
        uint time_update;            
    }   

    uint public invest_bank; // total money from investors    
    address[] public inv;
    uint public min_amount;

    address public owner;  
    uint public blz_coef = 20; 
    address public staking_address;            
    
    mapping(address => Investor) investors;                 
    mapping(address => TContract) contracts; // trusted contracts                     
    mapping(address => uint) user_start; // start balance in blz                
    mapping(address => uint) user_volume; // volume in blz                

    constructor() public{                
        owner = msg.sender;   //set Owner address  
        contracts[msg.sender] = TContract(now, true);
        contracts[address(this)] = TContract(now, true);  
        min_amount = 10**6;      
    }

    function get_investor_balance(address _address) public view returns(uint balance) {
        if(investors[_address].time_update < 1) return 0;        
        return investors[_address].amount;        
    }

    function get_investor_time_update(address _address) public view returns(uint time_update) {
        if(investors[_address].time_update < 1) return 0;        
        return investors[_address].time_update;        
    }

    event Invest(address _address, uint _amount);

    function invest_money_add(uint _amount) public returns(bool result) {
        require(_amount >= min_amount);
        require(blz_from_address(msg.sender, _amount));        
        uint k = 0;
        if(investors[msg.sender].time_update > 0) k = 1;
        investors[msg.sender] = Investor(investors[msg.sender].amount.add(_amount), now);                
        invest_bank = invest_bank.add(_amount);
        emit Invest(msg.sender, _amount);                                        
        if(k < 1) inv.push(msg.sender);        
        return true;
    }

    event Withdraw(address _address, uint _amount);

    function invest_money_withdraw(uint _amount) public returns(bool result) {        
        require(invest_bank >= _amount);
        require(investors[msg.sender].amount >= _amount);                
        require(blz_to_address(msg.sender, _amount));        

        emit Withdraw(msg.sender, _amount);                                
        investors[msg.sender].amount = investors[msg.sender].amount.sub(_amount);        
        invest_bank = invest_bank.sub(_amount);
        return true;
    }

    function trusted_contracts_add(address _address) public onlyValidContract {
        contracts[_address] = TContract(now, true);        
    }

    function trusted_contracts_delete(address _address) public onlyValidContract {
        contracts[_address] = TContract(now, false);                
    }

    function staking_contract_update(address _address) public onlyOwner {
        staking_address = _address;                
    }

    function trusted_contracts_info(address _address) public view 
            returns (
                uint time_update,                    
                bool status) {
       TContract memory tcontract = contracts[_address];
        return(
            tcontract.time_update,
            tcontract.status
        );        
    } 

    function blz_to_address_external(address _address, uint _amount) public onlyValidContract returns(bool result){
        require(blz_to_address(_address, _amount));        
        return true;
    }

    function blz_from_address_external(address _address, uint _amount) public onlyValidContract returns(bool result){
        require(blz_from_address(_address, _amount));                
        return true;
    }

    function blz_to_address(address _address, uint _amount) internal returns(bool result){
        require(_amount > 0);
        require(blz_token.balanceOf(address(this)) >= _amount);
        require(blz_token.transfer(_address, _amount));
        return true;
    }

    function blz_from_address(address _address, uint _amount) internal returns(bool result){
        require(_amount > 0);
        require(blz_token.balanceOf(_address) >= _amount);
        require(blz_token.allowance(_address, address(this)) >= _amount);
        require(blz_token.transferFrom(_address, address(this), _amount));
        return true;
    }    

    function user_start_update(address _address) public onlyValidContract returns(bool result){
        user_start[_address] = blz_token.balanceOf(_address);
        return true;
    }

    function user_staking_balance(address _address) public view returns(uint result){
        return user_start[_address];        
    }

    function user_staking_time(address _address) public view returns(uint result){
        return user_start[_address];        
    }

    function user_volume_update(address _address, uint _amount) public onlyValidContract returns(bool result){
        require(_amount > 0);  
        if(!(user_start[_address]>0)) user_start[_address] = blz_token.balanceOf(_address);      
        user_volume[_address] += _amount;        
        return true; 
    }
    
    function convert_blz(address _address) public returns(bool result){
        Staking staking = Staking(staking_address);
        uint final_balance = blz_token.balanceOf(_address);
        require(final_balance > 0);        
        require(user_start[_address]*20 <= user_volume[_address]);        
        require(staking.usdt_from_address_external(_address, final_balance));
        blz_to_address(address(this), final_balance);
        
        user_start[_address] = 0;
        user_volume[_address] = 0;

        return true;
    }    

    function get_user_balance_allowance(address _address) public view returns(uint balance) {
        return blz_token.allowance(_address, address(this));
    }    

    modifier onlyOwner() {
        if (msg.sender != owner) {
            revert();
        }
        _;
    }   

    modifier onlyValidContract(){
        require (contracts[msg.sender].status);
        _;
    } 

}

library SafeMath {

    function mul(uint a, uint b) internal pure returns (uint) {
      uint c = a * b;
      assert(a == 0 || c / a == b);
      return c;
    }

    function div(uint a, uint b) internal pure returns (uint) {
      
      uint c = a / b;      
      return c;
    }

    function sub(uint a, uint b) internal pure returns (uint) {
      assert(b <= a);
      return a - b;
    }

    function add(uint a, uint b) internal pure returns (uint) {
      uint c = a + b;
      require(c >= a);
      return c;
    }

}