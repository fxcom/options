pragma solidity ^0.5.10;
    
   // ----------------------------------------------------------------------------------------------
   // Developer Nechesov Andrey
   // Facebook.com/Nechesov 
   // Copyright (c) 2021. 
   // ----------------------------------------------------------------------------------------------    
   
  contract ERC20Interface {
      // Get the total token supply
      function totalSupply() external view returns (uint256);
   
      // Get the account balance of another account with address _owner
      function balanceOf(address _owner) external view returns (uint256);
   
      // Send _value amount of tokens to address _to
      function transfer(address _to, uint256 _value) external returns (bool);
   
      // Send _value amount of tokens from address _from to address _to
      function transferFrom(address _from, address _to, uint256 _value) external returns (bool);
   
      // Allow _spender to withdraw from your account, multiple times, up to the _value amount.
      // If this function is called again it overwrites the current allowance with _value.
      // this function is required for some functionality
      function approve(address _spender, uint256 _value) external returns (bool);
   
      // Returns the amount which _spender is still allowed to withdraw from _owner
      function allowance(address _owner, address _spender) external view returns (uint256);
   
      // Triggered when tokens are transferred.
      event Transfer(address indexed _from, address indexed _to, uint256 _value);

      // Triggered when tokens are transferred on reserve balance.
      event Transfer_to_reserve(address indexed _from, address indexed _to, uint256 _value);

      // Triggered when tokens are transferred on reserve balance.
      event Transfer_from_reserve(address indexed _from, uint256 _value);

      // Triggered when tokens are transferred on reserve balance.
      event Transfer_from_bet(address indexed _from, address indexed _to, uint256 _value);
   
      // Triggered whenever approve(address _spender, uint256 _value) is called.
      event Approval(address indexed _owner, address indexed _spender, uint256 _value);
  }
     
  contract BlzToken is ERC20Interface {

      string public constant symbol = "BLZ";
      string public constant name = "BLZ token";
      uint8 public constant decimals = 6;            
      
      uint256 private _totalSupply = 10*10**9*10**6;               

      using SafeMath for uint;
      
      // Owner of this contract
      address public owner;
   
      // Balances for each account
      mapping(address => uint256) balances;

      //Trading Volume in blz for each base account
      mapping(address => uint) user_volume; 

      // Start amount in BLZ for compare volume = amount*X
      mapping(address => uint256) user_amount;               

      // Reserve Balances for each account
      mapping(address => uint256) rbalances;      
   
      // Owner of account approves the transfer of an amount to another account
      mapping(address => mapping (address => uint256)) allowed;
   
      // Functions with this modifier can only be executed by the owner
      modifier onlyOwner() {
          if (msg.sender != owner) {
              revert();
          }
          _;
      } 
      
      constructor() public {        
        owner = msg.sender;        
        balances[owner] = _totalSupply;
      } 
   
      function totalSupply() public view returns (uint256) {
        return _totalSupply;
      }
   
      // What is the balance of a particular account?
      function balanceOf(address _owner) view public returns (uint256 balance) {
          return balances[_owner];
      }

      // What is the Reserve balance of a particular account?
      function rbalanceOf(address _owner) view public returns (uint256 balance) {
          return rbalances[_owner];
      }
   
       //Transfer the balance from addmin account to user account
       // Tradin Volume is 0
      
      function transfer(address _to, uint256 _amount) public onlyOwner returns (bool success) {          
        
          if (balances[msg.sender] >= _amount 
              && _amount > 0
              && balances[_to] + _amount > balances[_to]) {
              balances[msg.sender] -= _amount;
              balances[_to] += _amount;
              
              user_amount[_to] = balances[_to];
              user_volume[_to] = 0;

              emit Transfer(msg.sender, _to, _amount);
              return true;
          } else {
              return false;
          }
      }

      //Transfer the balance from admin account to user account from bets. 
      //Trading volume increase
      
      function transfer_from_bet(address _to, uint256 _amount) public onlyOwner returns (bool success) {          
        
          if (balances[msg.sender] >= _amount 
              && _amount > 0
              && balances[_to] + _amount > balances[_to]) {
              balances[msg.sender] -= _amount;
              balances[_to] += _amount;
              user_volume[_to] += _amount;
              emit Transfer_from_bet(msg.sender, _to, _amount);
              return true;
          } else {
              return false;
          }
      }
      

      // Transfer the balance from owner's account to another reserve account
      function transfer_to_reserve(address _to, uint256 _amount) public onlyOwner returns (bool success) {          
        
          if (balances[msg.sender] >= _amount 
              && _amount > 0
              && rbalances[_to] + _amount > rbalances[_to]) {
              balances[msg.sender] -= _amount;
              rbalances[_to] += _amount;
              emit Transfer_to_reserve(msg.sender, _to, _amount);
              return true;
          } else {
              return false;
          }
      }

      // Transfer the balance from reserve owner's account to owner base account
      function transfer_from_reserve(uint256 _amount) public returns (bool success) {          
        
          if (rbalances[msg.sender] >= _amount 
              && _amount > 0
              && balances[msg.sender] + _amount > rbalances[msg.sender]) {
              rbalances[msg.sender] -= _amount;
              balances[msg.sender] += _amount;

              user_amount[_to] = balances[_to];
              user_volume[msg.sender] = 0;
              
              emit Transfer_from_reserve(msg.sender, _amount);
              return true;
          } else {
              return false;
          }
      }
   
      // Send _value amount of tokens from address _from to address _to
      // The transferFrom method is used for a withdraw workflow, allowing contracts to send
      // tokens on your behalf, for example to "deposit" to a contract address and/or to charge
      // fees in sub-currencies; the command should fail unless the _from account has
      // deliberately authorized the sender of the message via some mechanism; we propose
      // these standardized APIs for approval:

      /*
      function transferFrom(
          address _from,
          address _to,
          uint256 _amount
      ) public returns (bool success) {         

         if (balances[_from] >= _amount
             && allowed[_from][msg.sender] >= _amount
             && _amount > 0
             && balances[_to] + _amount > balances[_to]) {
             balances[_from] -= _amount;
             allowed[_from][msg.sender] -= _amount;
             balances[_to] += _amount;
             emit Transfer(_from, _to, _amount);
             return true;
         } else {
             return false;
         }
     }
      */
     // Allow _spender to withdraw from your account, multiple times, up to the _value amount.
     // If this function is called again it overwrites the current allowance with _value.
     function approve(address _spender, uint256 _amount) public returns (bool success) {
         allowed[msg.sender][_spender] = _amount;
         emit Approval(msg.sender, _spender, _amount);
         return true;
     }
  
     function allowance(address _owner, address _spender) view public returns (uint256 remaining) {
         return allowed[_owner][_spender];
     }

     function TransferOwnership(address newOwner) onlyOwner public
    {
      owner = newOwner;
    }

 }

  /**
   * Math operations with safety checks
   */
  library SafeMath {
    function mul(uint a, uint b) internal pure returns (uint) {
      uint c = a * b;
      assert(a == 0 || c / a == b);
      return c;
    }

    function div(uint a, uint b) internal pure returns (uint) {
      
      uint c = a / b;      
      return c;
    }

    function sub(uint a, uint b) internal pure returns (uint) {
      assert(b <= a);
      return a - b;
    }

    function add(uint a, uint b) internal pure returns (uint) {
      uint c = a + b;
      require(c >= a);
      return c;
    }
    
  }