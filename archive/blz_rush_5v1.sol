pragma solidity ^0.5.10;        

// ----------------------------------------------------------------------------------------------
// Developer Nechesov Andrey: Facebook.com/Nechesov      
// ----------------------------------------------------------------------------------------------

contract Blz_staking {
    function usdt_to_address_external(address _address, uint _amount) public returns (bool);    
    function usdt_from_address_external(address _address, uint _amount) public returns (bool);
    function get_user_balance_allowance(address _address) public view returns(uint);    
}

contract Rush {

    using SafeMath for uint;  

    struct TOracle {                        
        uint time_update; 
        bool status;            
    }      

    struct Event {
        uint event_id;   
        string event_name;         
        uint type_id;        
        uint deviation_procent;
        uint time_gap;
        uint time_start;
        uint time_close;        
        uint min_bet;
        uint max_bet;                                
    }      

    struct Bet {
        uint bet_id;
        address user_address;        
        uint event_id;       
        uint amount; 
        uint direction;
        uint price_min;
        uint price_max;
        uint time_open; 
        uint time_close; 
        uint price_open;
        uint price_close;
        bool close;        
        uint coef;   
    }                     

    event Event_new(uint event_id);
    event Event_stop(uint event_id);
    event Bet_new(uint bet_id, uint event_id);
    event Bet_close(uint bet_id, uint event_id, bool close);

    //USDT-contract address: TNpP3csZPRPL39wS6s75fj1PBsrzStMbNj
    Blz_staking blz_staking = Blz_staking(0x41F055484082E86213745D916462871E5EE0CD994A);  

    mapping(address => TOracle) oracles; // trusted oracles                          

    address public owner;                   
    uint public decimal; 
    uint public commission; 
    uint public bet_reward_procent; //                  

    Bet public bet;

    uint[] public ds = [0,1179,2257,3139,3849,4332,4641,4821,4918,4965,4987];                        
    
    mapping (uint => mapping (uint => uint)) public total_bets_amount;         
    mapping (uint => mapping (uint => uint)) public total_bets_coef_amount;         

    mapping (uint => Event) public events; 
    uint public num_events = 0;

    mapping (uint => Bet[]) public bets;         
    mapping (uint => uint) public num_bets;         

    constructor() public{        
        owner = msg.sender;   //set Owner address      
        oracles[msg.sender] = TOracle(now, true);       
        decimal = 10; 
        commission = 25;
        bet_reward_procent = 2;          
    }
    
    function owner_change(address _owner_new) onlyOwner public {
        owner = _owner_new;
    }

    function bet_reward_procent_change(uint _procent) onlyOwner public returns(bool result){
        bet_reward_procent = _procent;
        return true;
    }

    function trusted_oracles_add(address _address) public onlyOwner {
        oracles[_address] = TOracle(now, true);        
    }

    function trusted_oracles_delete(address _address) public onlyOwner {
        oracles[_address] = TOracle(now, false);                
    }

    function trusted_oracles_info(address _address) public view 
            returns (
                uint time_update,                    
                bool status) {
       TOracle memory toracle = oracles[_address];
        return(
            toracle.time_update,
            toracle.status
        );        
    }                              

    function commission_change(uint _commission) onlyOwner public {
        require(_commission > 0);
        require(_commission < 100);

        commission = _commission;
    }  
    
    function event_add(string memory _event_name, uint _type_id, uint _deviation_procent, uint _time_gap, uint _min_bet, uint _max_bet) onlyOwner() public returns (uint event_id){
        
        require(bytes(_event_name).length > 0);
        require(_type_id > 0);
        require (_deviation_procent > 0);
        require (_time_gap > 0);
        require (_min_bet > 0);
        require (_max_bet > 0);
        

        uint time_start = now;

        event_id = num_events;
        num_events++;
        
        events[event_id] = Event(event_id, _event_name, _type_id, _deviation_procent, _time_gap, time_start, 0, _min_bet, _max_bet);            

        emit Event_new(event_id);                   
    }        

    function coef_get(uint _event_id, uint _price_open, uint _direction, uint _price_min, uint _price_max) view public returns (uint coef){            

        require(_price_min < _price_max);
        require(events[_event_id].deviation_procent > 0);

        if(_direction == 0){
            uint price_min_h = _price_open + (_price_open-_price_max);
            uint price_max_h = _price_open + (_price_open-_price_min);
            uint direction_h = 1;
            coef = coef_get(_event_id, _price_open, direction_h, price_min_h, price_max_h);
            return coef;
        }

        if(_price_min < _price_open) _price_min = _price_open;
        if(_price_max <= _price_open) return 0;        

        uint dp = events[_event_id].deviation_procent;

        uint k1 = 0;
        uint k2 = 0;                        
        
        k1 = (_price_max-_price_open)/(dp*_price_open/100/10)+1;
        k2 = (_price_min-_price_open)/(dp*_price_open/100/10);
        uint s = ds[k2]-ds[k1];

        uint p = 0;

        p = s/100*2;

        if(p < 10) p = 10;

        coef = 100/p*(100-commission);        

        if(coef < 100) coef = 100;

        return coef;        
        
    }    

    function bet_add(uint _event_id, uint _amount, uint _direction, uint _price_min, uint _price_max) public returns (bool result){

        require (events[_event_id].time_start < now);
        require (events[_event_id].time_close < 1);        
        require (_amount >= events[_event_id].min_bet);
        require (_amount <= events[_event_id].max_bet);        
        require (blz_staking.usdt_from_address_external(msg.sender, _amount));                

        uint time_open = now;
        uint time_close = time_open + events[_event_id].time_gap;
        num_bets[_event_id]++;
        bet = Bet(num_bets[_event_id]-1, msg.sender, _event_id, _amount, _direction, _price_min, _price_max, time_open, time_close, 0, 0, false, 0);            

        bets[_event_id].push(bet);     
        
        result = true;    
    }   

    function set_bet_price_open(uint _event_id, uint _bet_id, uint _price_open, uint _time_open) onlyOracle() public{

        require (!bets[_event_id][_bet_id].close);            
        require (bets[_event_id][_bet_id].time_open == _time_open);            
        require (_price_open > 0);                        

        bets[_event_id][_bet_id].price_open = _price_open;  

        uint direction = bets[_event_id][_bet_id].direction;                      
        uint price_min = bets[_event_id][_bet_id].price_min;                      
        uint price_max = bets[_event_id][_bet_id].price_max;                      

        bets[_event_id][_bet_id].coef = coef_get(_event_id, _price_open, direction, price_min, price_max);
        
    } 

    function set_bet_price_close(uint _event_id, uint _bet_id, uint _price_close, uint _time_close) onlyOracle() public{

        Bet memory b = bets[_event_id][_bet_id];

        require (!b.close);            
        require (b.time_close == _time_close);                        
        require (b.price_open > 0);
        require (_price_close > 0);                        

        bets[_event_id][_bet_id].price_close = _price_close;
        
        bet_close(_event_id,_bet_id);                         
        
    }                 

    function bet_pay(uint _event_id, uint _bet_id) private returns (bool result){            
        
        Bet memory b = bets[_event_id][_bet_id];        
        blz_staking.usdt_to_address_external(b.user_address, b.amount.mul(b.coef).div(100));
        return true;
    }

    function bet_close_manual(uint _event_id, uint _bet_id) public onlyOwner returns (bool result){
        require(bet_close(_event_id,_bet_id));
        return true;
    }

    function bet_close(uint _event_id, uint _bet_id) internal returns (bool result){        

        Bet memory b = bets[_event_id][_bet_id];                          

        require (!b.close);            
        require (b.price_open > 0);
        require (b.price_close > 0);
        require (b.time_close > 0);


        if(b.coef < 1){
            bets[_event_id][_bet_id].close = true;               
            emit Bet_close(_bet_id, _event_id, false); 
            return true;
        } 

        if(b.direction == 1) {
            if(b.price_close <= b.price_open) {
                bets[_event_id][_bet_id].close = true;   
                emit Bet_close(_bet_id, _event_id, false);     
                return true;
            }
        }

        if(b.direction == 0) {
            if(b.price_close >= b.price_open) {
                bets[_event_id][_bet_id].close = true;   
                emit Bet_close(_bet_id, _event_id, false);     
                return true;
            }             
        }

        if(b.direction == 1) {
            if((b.price_close > b.price_max)||(b.price_close < b.price_min)) {
                bets[_event_id][_bet_id].close = true;        
                emit Bet_close(_bet_id, _event_id, false);
                return true;
            }
        }

        if(b.direction == 0) {
            if((b.price_close < b.price_max)||(b.price_close < b.price_min)){
                bets[_event_id][_bet_id].close = true;        
                emit Bet_close(_bet_id, _event_id, false);
                return true;
            }             
        }
        
        if(!bet_pay(_event_id,_bet_id)) return false;            
        bets[_event_id][_bet_id].close = true;        
        emit Bet_close(_bet_id, _event_id, true);                   

        return true;
        
    }    

    function event_info(uint _event_id) public view 
            returns (
                uint event_id,   
                string memory event_name,                 
                uint type_id, 
                uint deviation_procent, 
                uint time_gap,
                uint time_start,
                uint time_close,
                uint min_bet,                                    
                uint max_bet
            ) {
        Event memory e = events[_event_id];                        

        return(                    
                e.event_id,
                e.event_name,
                e.type_id, 
                e.deviation_procent, 
                e.time_gap, 
                e.time_start,
                e.time_close, 
                e.min_bet, 
                e.max_bet
        );
    }

    function bet_info(uint _event_id, uint _bet_id) public view 
            returns (
                uint bet_id,
                address user_address,
                uint event_id,
                uint amount, 
                uint direction, 
                uint price_min,
                uint price_max,
                //uint time_open,
                //uint time_close,                                    
                uint price_open,
                uint price_close,
                bool close,
                uint coef
            ) {
        Bet memory b = bets[_event_id][_bet_id];                        

        return(
                b.bet_id, 
                b.user_address, 
                b.event_id,
                b.amount, 
                b.direction, 
                b.price_min, 
                b.price_max,
                //b.time_open, 
                //b.time_close, 
                b.price_open,
                b.price_close,
                b.close,
                b.coef
        );
    }        

    function event_stop(uint _event_id) onlyOwner() public returns (bool result){
        events[_event_id].time_close = now;
        result = true;
        emit Event_stop(_event_id);                   
    }                

    modifier onlyOwner() {
        if (msg.sender != owner) {
            revert();
        }
        _;
    }

    modifier onlyOracle() {
        require (oracles[msg.sender].status);
        _;
    }

}

library SafeMath {

    function mul(uint a, uint b) internal pure returns (uint) {
      uint c = a * b;
      assert(a == 0 || c / a == b);
      return c;
    }

    function div(uint a, uint b) internal pure returns (uint) {
      
      uint c = a / b;      
      return c;
    }

    function sub(uint a, uint b) internal pure returns (uint) {
      assert(b <= a);
      return a - b;
    }

    function add(uint a, uint b) internal pure returns (uint) {
      uint c = a + b;
      require(c >= a);
      return c;
    }

}