pragma solidity ^0.5.10;        

// ----------------------------------------------------------------------------------------------
// Developer Nechesov Andrey: Facebook.com/Nechesov      
// ----------------------------------------------------------------------------------------------

import "./formulas/options_formulas.sol";

contract Blz_staking {
    function usdt_to_address_external(address _address, uint _amount) public returns (bool);    
    function usdt_from_address_external(address _address, uint _amount) public returns (bool);
    function get_user_balance_allowance(address _address) public view returns(uint);    
}

contract Options is Formulas {  

    using SafeMath for uint;  

    struct TOracle {                        
        uint time_update; 
        bool status;            
    }    

    struct Event {
        uint event_id; 
        string event_name;       
        uint time_start;
        uint time_close;
        uint time_gap;
        uint min_bet;
        uint max_bet;                        
        uint[] coefs; 
    }      

    struct Bet {
        uint bet_id;
        address user_address;
        uint event_id;
        uint time_open; 
        uint time_close; 
        uint price_open;
        uint price_close;
        uint out_id;      // CALL = 0; PUT = 1;     
        uint bet_amount;                                    
        uint bet_coef;   // bet_coef > 100
    }                     

    event Event_new(uint event_id);
    event Event_stop(uint event_id);
    event Bet_new(uint bet_id, uint event_id);
    event Bet_close(uint bet_id, uint event_id, bool status);
    
    //USDT-contract address: TNpP3csZPRPL39wS6s75fj1PBsrzStMbNj
    Blz_staking blz_staking = Blz_staking(0x41F055484082E86213745D916462871E5EE0CD994A);  

    mapping(address => TOracle) oracles; // trusted oracles                       

    address public owner;                             

    Bet public bet;         
    uint public bet_reward_procent; // 

    mapping (uint => Event) public events; 
    uint public num_events = 0;

    mapping (uint => Bet[]) public bets;         
    mapping (uint => uint) public num_bets;         

    constructor() public{        
        owner = msg.sender;   //set Owner address              
        bet_reward_procent = 2;     
        oracles[msg.sender] = TOracle(now, true);       
    }
    
    function owner_change(address _owner_new) onlyOwner public {
        owner = _owner_new;
    } 

    function bet_reward_procent_change(uint _procent) onlyOwner public returns(bool result){
        bet_reward_procent = _procent;
        return true;
    } 

    function trusted_oracles_add(address _address) public onlyOwner {
        oracles[_address] = TOracle(now, true);        
    }

    function trusted_oracles_delete(address _address) public onlyOwner {
        oracles[_address] = TOracle(now, false);                
    }

    function trusted_oracles_info(address _address) public view 
            returns (
                uint time_update,                    
                bool status) {
       TOracle memory toracle = oracles[_address];
        return(
            toracle.time_update,
            toracle.status
        );        
    }       

    function get_balance() public view returns(uint result){
        return blz_staking.get_user_balance_allowance(msg.sender);   
    }
    
    function event_add(string memory _event_name, uint _time_gap, uint _min_bet, uint _max_bet, uint[] memory _coefs) onlyOwner() public returns (uint event_id){
        require(bytes(_event_name).length > 0);
        require(_coefs.length == 2);
        require (_coefs[0] > 100);
        require (_coefs[1] > 100);        

        uint time_start = now;

        event_id = num_events;
        num_events++;
        
        events[event_id] = Event(event_id, _event_name, time_start, 0, _time_gap, _min_bet, _max_bet, _coefs);            

        emit Event_new(event_id);                   
        return event_id;
    }        

    function coef_get(uint _event_id, uint _out_id) view public returns (uint coef){            
        coef = events[_event_id].coefs[_out_id];
        return coef;
    }

    function coef_update(uint _event_id, uint[] memory _coefs) onlyOwner() public returns (bool result){

        require(events[_event_id].coefs.length == _coefs.length);

        for (uint i=0; i<events[_event_id].coefs.length; i++) {                
          events[_event_id].coefs[i] = _coefs[i];
        }            

        result = true;
        
        return result;
    }

    function bet_add(uint _event_id,uint _out_id, uint _amount) public returns (bool result){

        require (events[_event_id].time_start < now);
        require (events[_event_id].time_close < 1);        
        require (_amount >= events[_event_id].min_bet);
        require (_amount <= events[_event_id].max_bet);                 
        require(blz_staking.usdt_from_address_external(msg.sender, _amount));                
        
        uint p_win = Formulas.binary_call();
        require(p_win > 0);
        uint bet_coef = 1/p_win;

        num_bets[_event_id]++;
        bet = Bet(num_bets[_event_id]-1, msg.sender, _event_id, now, 0, 0, 0, _out_id, _amount, bet_coef);            

        bets[_event_id].push(bet);     

        emit Bet_new(bet.bet_id, bet.event_id);                   
        result = true;
        return result;    
    }   

    function set_bet_price_open(uint _event_id, uint _bet_id, uint _price_open, uint _time_open) onlyOracle() public returns(bool result){

        require(bets[_event_id][_bet_id].time_close < 1);            
        require(bets[_event_id][_bet_id].time_open == _time_open);            
        require(_price_open > 0);                        

        bets[_event_id][_bet_id].price_open = _price_open;                         
        
        return true;    
    } 

    function set_bet_price_close(uint _event_id, uint _bet_id, uint _price_close, uint _time_close) onlyOracle() public returns(bool result){

        require(bets[_event_id][_bet_id].time_close < 1);            
        require(bets[_event_id][_bet_id].time_open + events[_event_id].time_gap == _time_close);                        
        require(bets[_event_id][_bet_id].price_open > 0);
        require(_price_close > 0);                        

        bets[_event_id][_bet_id].price_close = _price_close;                         

        return true;
        
    }                 

    function bet_pay(uint _event_id, uint _bet_id) private returns (bool result){            
        
        Bet memory b = bets[_event_id][_bet_id];
        uint amount = b.bet_amount.mul(b.bet_coef).div(100);
        require(blz_staking.get_user_balance_allowance(address(this)) >= amount);                            
        require(blz_staking.usdt_to_address_external(b.user_address, amount));        

        result = true;
        return result;
    }

    function bet_close(uint _event_id, uint _bet_id) onlyOwner public payable returns (bool result){

        Bet memory b = bets[_event_id][_bet_id];

        require(b.time_close < 1);            
        require(b.price_open > 0);
        require(b.price_close > 0);          

        uint out_id = 2;            

        if(b.price_open < b.price_close) {
            out_id = 0;                
        }                                             

        if(b.price_open > b.price_close) {
            out_id = 1;                
        }                                             

        if(b.out_id != out_id) {            
            emit Bet_close(_bet_id, _event_id, false);                   
            return false;
        }
        
        require(bet_pay(_event_id,_bet_id));            
        
        bets[_event_id][_bet_id].time_close = now;              
        
        emit Bet_close(_bet_id, _event_id, true);                   

        return true;
        
    }    

    function event_stop(uint _event_id) onlyOwner() public returns (bool result){
        events[_event_id].time_close = now;
        result = true;
        emit Event_stop(_event_id);                   
    }

    function event_info(uint _event_id) public view 
            returns (
                uint event_id,                    
                string memory event_name,
                uint time_start, 
                uint time_close, 
                uint time_gap,
                uint min_bet,
                uint max_bet,
                uint coef0,                                    
                uint coef1
            ) {
        Event memory e = events[_event_id];                        

        return(                    
                e.event_id,
                e.event_name,
                e.time_start, 
                e.time_close, 
                e.time_gap, 
                e.min_bet,
                e.max_bet, 
                e.coefs[0], 
                e.coefs[1]
        );
    }

    function bet_info(uint _event_id, uint _bet_id) public view 
            returns (
                uint bet_id,
                address user_address,
                uint event_id,
                uint time_open, 
                uint time_close, 
                uint price_open,
                uint price_close,
                uint out_id,
                uint bet_amount,                                    
                uint bet_coef
            ) {
        Bet memory b = bets[_event_id][_bet_id];                        

        return(
                b.bet_id, 
                b.user_address, 
                b.event_id,
                b.time_open, 
                b.time_close, 
                b.price_open, 
                b.price_close,
                b.out_id, 
                b.bet_amount, 
                b.bet_coef
        );
    }                            

    modifier onlyOwner() {
        if (msg.sender != owner) {
            revert();
        }
        _;
    }

    modifier onlyOracle() {
        require (oracles[msg.sender].status);
        _;
    }

}

library SafeMath {

    function mul(uint a, uint b) internal pure returns (uint) {
      uint c = a * b;
      assert(a == 0 || c / a == b);
      return c;
    }

    function div(uint a, uint b) internal pure returns (uint) {
      
      uint c = a / b;      
      return c;
    }

    function sub(uint a, uint b) internal pure returns (uint) {
      assert(b <= a);
      return a - b;
    }

    function add(uint a, uint b) internal pure returns (uint) {
      uint c = a + b;
      require(c >= a);
      return c;
    }

}