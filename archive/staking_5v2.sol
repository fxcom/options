pragma solidity ^0.5.10;        

// ----------------------------------------------------------------------------------------------
// Developer Nechesov Andrey: Facebook.com/Nechesov      
// ----------------------------------------------------------------------------------------------

contract TetherToken {
    function balanceOf(address who) external view returns (uint256);    
    function transfer(address to, uint256 value) public returns (bool);
    function transferFrom(address from, address to, uint256 value) public returns (bool);
    function allowance(address _owner, address _spender) external view returns (uint256);
}

contract Staking {  

    using SafeMath for uint;

    TetherToken tether_token = TetherToken(0x41c69c7aedf15e83a216193cfc0588a658c91c1e36aa0e35cf);            

    event Invest(address _address, uint _amount);
    event Withdraw(address _address, uint _amount);
    event Daily_procent(uint _daily_procent);

    struct Investor {                        
        uint balance; 
        uint time_update;            
    }           
    
    address public owner; 
    uint private daily_procent;
    uint private current_pool;
    uint private total_staked;
    mapping(address => Investor) investors;                     

    constructor() public{        
        owner = msg.sender;   //set owner address              
        daily_procent = 5;    //5 = 0.05 % in day                  
        current_pool = 0;
        total_staked = 0;
    }    

    function invest_money_add(uint _amount) public returns(bool result) {
        require(_amount > 0);
        require(tether_token.balanceOf(msg.sender) >= _amount);
        require(tether_token.allowance(msg.sender, address(this)) >= _amount);
        require(tether_token.transferFrom(msg.sender, address(this), _amount));
        investors[msg.sender] = Investor(investors[msg.sender].balance.add(_amount), now);
        current_pool = current_pool.add(_amount);
        total_staked = total_staked + 1;
        emit Invest(msg.sender, _amount);                                
        return true;
    }

    function get_balance(address _address) public view returns(uint balance) {
        if(investors[_address].time_update < 1) return 0;
        uint total_days = (now-investors[_address].time_update)/(24*60*60);        
        if(total_days == 0) return investors[_address].balance;
        balance = investors[_address].balance + total_days*daily_procent/(100*100);        
    }    

    function get_reward_info(address _address) public view returns(uint) {
        if(investors[_address].time_update < 1) return 0;
        uint balance = get_balance(_address);
        return balance - investors[_address].balance;
    }

    function get_total_staked() public view returns(uint) {
        return total_staked;
    }    

    function get_pool() public view returns(uint) {
        return current_pool;
    }    

    function get_procent() public view returns(uint) {
        return daily_procent;
    }    

    function invest_money_withdraw(uint _amount) public returns(bool result) {

        uint balance = get_balance(msg.sender);

        require(_amount > 0);                        
        require(balance >= _amount);                
        require(tether_token.balanceOf(address(this)) >= _amount);        

        investors[msg.sender].balance = balance;
        investors[msg.sender].time_update = now;
        tether_token.transfer(msg.sender, _amount);         
        investors[msg.sender].balance = investors[msg.sender].balance.sub(_amount);           
        current_pool = current_pool.sub(_amount);        

        emit Withdraw(msg.sender, _amount);                                
        return true;
    }    

    function investor_info(address _address) public view 
            returns (                
                uint _balance,                    
                uint _time_update) {
        Investor memory investor = investors[_address];
        return(            
            investor.balance,
            investor.time_update
        );        
    }

    function daily_procent_change(uint _daily_procent) public onlyOwner {        
        daily_procent = daily_procent; //5 = 0.05 % in day 
        emit Daily_procent(_daily_procent);
    }

    function owner_change(address _address) public onlyOwner {
        owner = _address;
    }

    modifier onlyOwner() {
        if (msg.sender != owner) {
            revert();
        }
        _;
    }    

}

library SafeMath {

    function mul(uint a, uint b) internal pure returns (uint) {
      uint c = a * b;
      assert(a == 0 || c / a == b);
      return c;
    }

    function div(uint a, uint b) internal pure returns (uint) {
      
      uint c = a / b;      
      return c;
    }

    function sub(uint a, uint b) internal pure returns (uint) {
      assert(b <= a);
      return a - b;
    }

    function add(uint a, uint b) internal pure returns (uint) {
      uint c = a + b;
      require(c >= a);
      return c;
    }

}