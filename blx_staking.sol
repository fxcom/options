pragma solidity ^0.5.10;        

// ----------------------------------------------------------------------------------------------
// Developer Nechesov Andrey: Facebook.com/Nechesov      
// ----------------------------------------------------------------------------------------------
/*
contract TetherToken {
    function balanceOf(address who) external view returns (uint256);    
    function transfer(address to, uint256 value) public returns (bool);
    function transferFrom(address from, address to, uint256 value) public returns (bool);
    function allowance(address _owner, address _spender) external view returns (uint256);
}
*/

contract BlxToken {
    function balanceOf(address who) external view returns (uint256);    
    function transfer(address to, uint256 value) public returns (bool);
    function transferFrom(address from, address to, uint256 value) public returns (bool);
    function allowance(address _owner, address _spender) external view returns (uint256);
}

contract Blx_staking {      

    using SafeMath for uint;

    //BLX-contract address: 
    BlxToken blx_token = BlxToken(0xDfd43918cAfc3aF972edC7Ed73E1563e23a07842);    

    struct TContract {                        
        uint time_update; 
        bool status;            
    }    

    struct Investor {                        
        uint amount; 
        uint staking_balance;
        uint time_update;            
    }   

    uint public invest_bank; // total money from investors    
    address[] public inv;
    uint public min_amount;

    address public owner;      
    //address public staking_address;            
    
    mapping(address => Investor) investors;                 
    mapping(address => TContract) contracts; // trusted contracts                         

    constructor() public{                
        owner = msg.sender;   //set Owner address  
        contracts[msg.sender] = TContract(now, true);
        contracts[address(this)] = TContract(now, true);  
        min_amount = 10**6;      
    }

    function get_investor_balance(address _address) public view returns(uint balance) {
        if(investors[_address].time_update < 1) return 0;        
        return investors[_address].amount;        
    }

    function get_investor_time_update(address _address) public view returns(uint time_update) {
        if(investors[_address].time_update < 1) return 0;        
        return investors[_address].time_update;        
    }

    function get_staking_balance(address _address) public view returns(uint balance) {
        if(investors[_address].time_update < 1) return 0;        
        return investors[_address].amount;        
    }    

    event Add(address _address, uint _amount);

    function invest_money_add(uint _amount) public returns(bool result) {
        require(_amount >= min_amount);
        require(blx_from_address(msg.sender, _amount));        
        uint k = 0;
        if(investors[msg.sender].time_update > 0) k = 1;
        investors[msg.sender].amount = investors[msg.sender].amount.add(_amount);        
        invest_bank = invest_bank.add(_amount);
        emit Add(msg.sender, _amount);                                        
        if(k < 1) inv.push(msg.sender);        
        return true;
    }

    event Withdraw(address _address, uint _amount);

    function invest_money_withdraw(uint _amount) public returns(bool result) {        
        require(invest_bank >= _amount);
        require(investors[msg.sender].amount >= _amount);                
        require(blx_to_address(msg.sender, _amount));        

        emit Withdraw(msg.sender, _amount);                                
        investors[msg.sender].amount = investors[msg.sender].amount.sub(_amount);        
        invest_bank = invest_bank.sub(_amount);
        return true;
    }

    event Stake(address _address, uint _amount);

    function invest_stake(uint _amount) public onlyOwner returns(bool result) {                
        require(investors[msg.sender].amount >= _amount);                
        emit Stake(msg.sender, _amount);                                
        investors[msg.sender].amount = investors[msg.sender].amount.sub(_amount);        
        investors[msg.sender].staking_balance = investors[msg.sender].staking_balance.add(_amount);                
        investors[msg.sender].time_update = now;        
        return true;
    }

    event Unstake(address _address, uint _amount);

    function invest_unstake(uint _amount) public onlyValidContract returns(bool result) {                
        require(investors[msg.sender].staking_balance >= _amount);                
        emit Unstake(msg.sender, _amount);                                        
        investors[msg.sender].staking_balance = investors[msg.sender].staking_balance.sub(_amount);        
        investors[msg.sender].amount = investors[msg.sender].amount.add(_amount);                
        investors[msg.sender].time_update = now;
        return true;
    }

    function invest_balance(address _address) public view returns(uint balance) {                                
        return investors[_address].amount;
    }

    function invest_stake_balance(address _address) public view returns(uint balance) {                                
        return investors[_address].staking_balance;
    }

    function trusted_contracts_add(address _address) public onlyValidContract {
        contracts[_address] = TContract(now, true);        
    }

    function trusted_contracts_delete(address _address) public onlyValidContract {
        contracts[_address] = TContract(now, false);                
    }    

    function trusted_contracts_info(address _address) public view 
            returns (
                uint time_update,                    
                bool status) {
       TContract memory tcontract = contracts[_address];
        return(
            tcontract.time_update,
            tcontract.status
        );        
    } 

    function blx_to_address_external(address _address, uint _amount) public onlyValidContract returns(bool result){
        require(blx_to_address(_address, _amount));        
        return true;
    }

    function blx_from_address_external(address _address, uint _amount) public onlyValidContract returns(bool result){
        require(blx_from_address(_address, _amount));                
        return true;
    }

    function blx_to_address(address _address, uint _amount) internal returns(bool result){
        require(_amount > 0);
        require(blx_token.balanceOf(address(this)) >= _amount);
        require(blx_token.transfer(_address, _amount));
        return true;
    }

    function blx_from_address(address _address, uint _amount) internal returns(bool result){
        require(_amount > 0);
        require(blx_token.balanceOf(_address) >= _amount);
        require(blx_token.allowance(_address, address(this)) >= _amount);
        require(blx_token.transferFrom(_address, address(this), _amount));
        return true;
    }    

    function get_user_balance_allowance(address _address) public view returns(uint balance) {
        return blx_token.allowance(_address, address(this));
    }    

    modifier onlyOwner() {
        if (msg.sender != owner) {
            revert();
        }
        _;
    }   

    modifier onlyValidContract(){
        require (contracts[msg.sender].status);
        _;
    } 

}

library SafeMath {

    function mul(uint a, uint b) internal pure returns (uint) {
      uint c = a * b;
      assert(a == 0 || c / a == b);
      return c;
    }

    function div(uint a, uint b) internal pure returns (uint) {
      
      uint c = a / b;      
      return c;
    }

    function sub(uint a, uint b) internal pure returns (uint) {
      assert(b <= a);
      return a - b;
    }

    function add(uint a, uint b) internal pure returns (uint) {
      uint c = a + b;
      require(c >= a);
      return c;
    }

}