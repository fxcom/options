//import "./Forwarder.sol";

contract Forwarder {
  address public destination;

  constructor(address _destination) public {
    destination = _destination;
  }

  function flushERC20(address tokenContractAddress) public {
    ERC20 tokenContract = IERC20(tokenContractAddress);
    uint256 forwarderBalance = tokenContract.balanceOf(address(this));
    tokenContract.transfer(destination, forwarderBalance);
  }
}

contract ForwarderFactory {

  function cloneForwarder(address forwarder, uint256 salt)
      public returns (Forwarder clonedForwarder) {
    address clonedAddress = createClone(forwarder, salt);
    Forwarder parentForwarder = Forwarder(forwarder);
    clonedForwarder = Forwarder(clonedAddress);
    clonedForwarder.init(parentForwarder.destination());
  }

  function createClone(address target, uint256 salt) private returns (address result) {
    bytes20 targetBytes = bytes20(target);
    assembly {
      let clone := mload(0x40)
      mstore(clone, 0x3d602d80600a3d3981f3363d3d373d3d3d363d73000000000000000000000000)
      mstore(add(clone, 0x14), targetBytes)
      mstore(add(clone, 0x28), 0x5af43d82803e903d91602b57fd5bf30000000000000000000000000000000000)
      result := create2(0, clone, 0x37, salt)
    }
  }

}