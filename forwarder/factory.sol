pragma solidity ^0.5.10;

contract Factory {
  event Deployed(address addr, uint256 salt);

  address public last_address;

  function deploy(bytes memory code, uint256 salt) public {  
    address addr;
    assembly {
      addr := create2(0, add(code, 0x20), mload(code), salt)
      if iszero(extcodesize(addr)) {
        revert(0, 0)
      }
    }

    last_address = addr;

    emit Deployed(addr, salt);
  }
}